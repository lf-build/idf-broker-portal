// getRedirectionUrl
export default (code, applicationNumber, tags) => {
  let urlTo = '';
  try {
    switch (code) {
      case '200.10':
        urlTo = `/application/${applicationNumber}`;
        break;
      case '200.20':
      case '200.30':
      case '200.40':
      case '200.50':
        urlTo = `/verification/${applicationNumber}`;
        break;
      case '300.10':
        urlTo = `/pricing/${applicationNumber}`;
        break;
      case '400.10':
      case '500.10':
        if (tags && tags.length >= 0 && (tags.indexOf('Pending Signature') > -1 || tags.indexOf('Pending Documents') > -1)) {
          urlTo = `/agreement/${applicationNumber}`;
        } else {
          urlTo = `/finalization/${applicationNumber}`;
        }
        break;
      case '500.20':
        urlTo = `/finalization/${applicationNumber}`;
        break;
      case '600.10':
        urlTo = `/funded/${applicationNumber}`;
        break;
      case '700.10':
      case '700.20':
      case '700.30':
      case '700.40':
      case '700.50':
        urlTo = `/declined/${applicationNumber}`;
        break;
      default:
        urlTo = '/dashboard';
    }
  } catch (e) {
    // console.log(e);
  }
  return urlTo;
};
