
import isRegexMatch from './isRegexMatch';
import { abaRegex } from './regexList';

export const isRoutingNumber = (name) => (value) => isRegexMatch(value, abaRegex) ? undefined : `Please enter a valid ${name}`;
