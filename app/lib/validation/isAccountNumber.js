
import isRegexMatch from './isRegexMatch';
import { accNumberRegex } from './regexList';

export const isAccountNumber = (name) => (value) => isRegexMatch(value, accNumberRegex) ? undefined : `Please enter a valid ${name}`;
