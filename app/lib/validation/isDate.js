import isRegexMatch from './isRegexMatch';
import { dateRegex } from './regexList';

export const isDate = (name) => (value) => isRegexMatch(value, dateRegex) ? undefined : `Please enter a valid ${name}`;
