import isInRange from './isInRange';
export const isInLength = (min, max, exclude) => (value) => isInRange(value, min, max, exclude) ? undefined : `Please enter at least ${min} characters`;
