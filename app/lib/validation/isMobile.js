import isRegexMatch from './isRegexMatch';
import { mobileRegex } from './regexList';

export const isMobile = (name) => (value) => isRegexMatch(value.replace(/ /g, ''), mobileRegex) ? undefined : `Please enter a valid ${name}`;
