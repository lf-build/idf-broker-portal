/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */

import { combineReducers } from 'redux-immutable';
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import authReducer from 'sagas/auth/reducer';
import traceReducer from 'sagas/session/reducer';
import languageProviderReducer from 'containers/LanguageProvider/reducer';
import { reducer as formReducer } from 'redux-form/immutable';
// import { SHOW_POPUP, PAUSE, START } from 'sagas/session/constants';

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const routeInitialState = fromJS({ locationBeforeTransitions: null });

/**
 * Merge route into the global application state
 */
function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
      /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({ locationBeforeTransitions: action.payload });
    default:
      return state;
  }
}


/* const traceInitialState = fromJS({ time: new Date() });
function traceReducer(state = traceInitialState, action) {
  switch (action.type) {
    case SHOW_POPUP:
      state.merge({ show: true });
      break;
    case PAUSE:
      state.merge({ enabled: false });
      break;
    case START:
      state.merge({ enabled: true });
      break;
    default:
      state.merge({ show: false });
  }
  return state.merge({ time: new Date() });
}
 */
/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer(asyncReducers) {
  const appReducer = combineReducers({
    trace: traceReducer,
    auth: authReducer,
    form: formReducer,
    route: routeReducer,
    language: languageProviderReducer,
    ...asyncReducers,
  });

  /* istanbul ignore next */
  return (state, action) => {
    if (action.type === 'RESET') {
      return appReducer(action.state, action);
    }
    return appReducer(state, action);
  };
}
