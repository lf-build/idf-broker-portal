import { createSelector } from 'reselect';

/**
 * Direct selector to the personalDetails state domain
 */
const selectPersonalDetailsDomain = () => (state) => state.get('personalDetails');

/**
 * Other specific selectors
 */


/**
 * Default selector used by PersonalDetails
 */

const makeSelectPersonalDetails = () => createSelector(
  selectPersonalDetailsDomain(),
  (substate) => substate.toJS()
);

export default makeSelectPersonalDetails;
export {
  selectPersonalDetailsDomain,
};
