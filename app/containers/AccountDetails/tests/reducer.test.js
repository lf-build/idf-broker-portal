
import { fromJS } from 'immutable';
import accountDetailsReducer from '../reducer';

describe('accountDetailsReducer', () => {
  it('returns the initial state', () => {
    expect(accountDetailsReducer(undefined, {})).toEqual(fromJS({}));
  });
});
