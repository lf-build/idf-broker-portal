/*
 * AccountDetails Messages
 *
 * This contains all the text for the AccountDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AccountDetails.header',
    defaultMessage: 'This is AccountDetails container !',
  },
});
