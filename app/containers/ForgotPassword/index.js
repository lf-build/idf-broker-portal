/*
 *
 * ForgotPassword
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import { createStructuredSelector } from 'reselect';
import Layout from 'components/PublicLayout';
import AlertMessage from 'components/AlertMessage';
import authCheck from 'components/Helper/authCheck';
import { isEmail } from 'lib/validation/isEmail';
import { required } from 'lib/validation/required';
import makeSelectForgotPassword from './selectors';
import messages from './messages';

export class ForgotPassword extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, userName: '' };
  }
  modifyJason = (values) => {
    this.setState({
      userName: values.username,
    });
    const request = {
      ...values,
      realm: 'broker-portal',
    };
    return request;
  };
  afterSubmitHandle = (e) => {
    if (e) {
      this.setState({ isError: true, message: 'The email you typed is not associated with a partner account.', isVisible: true });
      authCheck(this.props.dispatch)(e);
      return;
    }
    this.setState({ isError: false, message: 'Email sent successfully', isVisible: true });
  };
  render() {
    const parsedMessages = localityNormalizer(messages);
    return (
      <Layout>
        <div className="container">
          <div className="row">
            <div className="login_wraper">
              <div className="col-md-6 col-sm-8 login-content">
                <div className="main_login">
                  <h3>Forgot Password</h3>
                  <div className="main_logininner">
                    <Form initialValuesBuilder={() => ({ username: this.state.userName })} afterSubmit={this.afterSubmitHandle} name="forgotPassword" action="authorization/identity/password-reset-init" payloadBuilder={this.modifyJason}>
                      <TextField name="username" {...parsedMessages.email} validate={[required('Email'), isEmail('email')]} />
                      <button type="submit" className="loin_btn">Submit </button>
                    </Form>
                    {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
                    <p><Link to="/login">Click here</Link> to redirect to login page</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

ForgotPassword.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ForgotPassword: makeSelectForgotPassword(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
