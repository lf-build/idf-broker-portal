/*
 * ForgotPassword Messages
 *
 * This contains all the text for the ForgotPassword component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ForgotPassword.header',
    defaultMessage: 'This is ForgotPassword container !',
  },
  emailPlaceholder: {
    id: 'app.components.ForgotPassword.emailPlaceholder',
    defaultMessage: 'Enter Your Email',
  },
});
