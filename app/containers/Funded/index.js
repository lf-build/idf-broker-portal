/*
 *
 * Funded
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter, browserHistory } from 'react-router';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import FinalApplicationDetail from 'components/FinalApplicationDetail';
import ApplicantDetail from 'components/ApplicantDetail';
import { executeAPI } from 'components/Helper/execute';
import makeAuthSelector from 'sagas/auth/selectors';
import img from 'assets/images/success-icon.png';
import { createStructuredSelector } from 'reselect';
import makeSelectFunded from './selectors';

export class Funded extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.location.pathname.split('/')[2] };
  }
  componentDidMount() {
    const payLoad = {
      applicationNumber: this.state.applicationNumber,
    };
    executeAPI('api/finalization/get-finalization', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
        if (response.data.status.code === 400) {
          if (!this.props.auth.user) {
            browserHistory.replace('/login');
          } else {
            browserHistory.replace('/dashboard');
          }
        }
      } else {
        this.setState({ Funded: response.data.body });
      }
    });
  }
  render() {
    if (!this.state || !this.state.Funded) {
      return <Spinner />;
    }
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="4" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Deal funded successfully</b>
                <p>Your client, {this.state.Funded.businessName}, was funded successfully. See below for deal summary.</p>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="finalization-wrap">
              <div className="col-lg-10 no-pad">
                <div className="final-head">
                  <h4>{'Here\'s your deal summary:'}</h4>
                </div>
                <div className="col-lg-5 col-sm-6 ">
                  <FinalApplicationDetail applicationDetails={this.state.Funded} />
                </div>
                <div className="col-lg-5 col-sm-6">
                  <div className="success-agreement">
                    <div className="icon-bx">
                      <img src={img} alt="" />
                    </div>
                    <div className="agree-text">
                      <h3>Deal funded successfully</h3>
                      <a href="/dashboard">Back to Dashboard</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Funded.propTypes = {
  location: PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  Funded: makeSelectFunded(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Funded));
