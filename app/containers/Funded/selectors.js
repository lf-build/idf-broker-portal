import { createSelector } from 'reselect';

/**
 * Direct selector to the funded state domain
 */
const selectFundedDomain = () => (state) => state.get('funded');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Funded
 */

const makeSelectFunded = () => createSelector(
  selectFundedDomain(),
  (substate) => substate.toJS()
);

export default makeSelectFunded;
export {
  selectFundedDomain,
};
