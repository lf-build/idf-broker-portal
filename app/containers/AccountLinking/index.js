/*
 *
 * AccountLinking
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router';
import { executeAPI } from 'components/Helper/execute';
import Layout from 'components/PublicLayout';
import Spinner from 'components/Spinner';
import Broker from 'components/Broker';
import AlertMessage from 'components/AlertMessage';
import LinkAccountingSystem from 'components/LinkAccountingSystem';
import LinkBankAccount from 'components/LinkBankAccount';
import CreditReview from 'components/CreditReview';
import AccountLinkingDone from 'components/AccountLinkingDone';
import makeSelectAccountLinking from './selectors';

export class AccountLinking extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { apiCall: false, isError: false, isVisible: false, message: '' };
  }
  componentDidMount() {
    this.loadData();
    window.applicationNumber = this.props.location.pathname.split('/')[3];
  }
  apiCallStatus = (status) => {
    if (status === 1) {
      this.state.apiCall = true;
      this.setState(this.state);
    } else if (status === 2) {
      this.state.apiCall = false;
      this.setState(this.state);
    } else {
      this.state.isError = false;
      this.state.isVisible = false;
      this.state.message = 'Something went wrong. Please try again later.';
      this.state.apiCall = false;
      this.setState(this.state);
    }
  }
  skipCriskco = () => {
    this.apiCallStatus(1);
    const payLoad = {
      applicationNumber: this.props.location.pathname.split('/')[3],
    };
    executeAPI('api/account-linking/skip-criskco', payLoad, undefined, undefined, false, (response) => {
      if (response.error) {
        this.apiCallStatus(3);
      } else {
        setTimeout(this.loadData(), 500);
      }
    });
  }
  loadData = () => {
    const payLoad = { applicationNumber: this.props.location.pathname.split('/')[3],
      token: this.props.location.pathname.split('/')[4] };
    executeAPI('api/account-linking/get-account-linking', payLoad, undefined, undefined, false, (response) => {
      if (response.error) {
        this.apiCallStatus(3);
      } else {
        this.apiCallStatus(2);
        this.setState({ accountLinking: response.data.body });
      }
    });
  }
  render() {
    if (!this.state || !this.state.accountLinking) {
      return <Spinner />;
    }
    const { IsTokenValid, accounting, broker, banking, creditReview } = this.state.accountLinking;
    return (
      <Layout>
        { IsTokenValid && <div className="acount_wraper">
          <Broker broker={broker} />
          <div className="col-md-6 account-contain">
            {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
          </div>
          { !accounting.status && <LinkAccountingSystem apiCallStatus={this.apiCallStatus} apiCall={this.state.apiCall} loadData={this.loadData} skipCriskco={this.skipCriskco} /> }
          { (accounting.status && !banking.status) && <LinkBankAccount apiCallStatus={this.apiCallStatus} apiCall={this.state.apiCall} loadData={this.loadData} location={this.props.location} banking={banking} /> }
          { (accounting.status && banking.status && !creditReview.status) && <CreditReview apiCallStatus={this.apiCallStatus} apiCall={this.state.apiCall} loadData={this.loadData} location={this.props.location} />}
          { (accounting.status && banking.status && creditReview.status) && <AccountLinkingDone />}
        </div>
      }{ !IsTokenValid && <div className="acount_wraper">
        <Broker broker={broker} />
        <div className="col-md-6 account-contain">
          <div className="col-md-12">
            <div className="link-wrap link-expire">
              <span className="token-msg">Your link has expired. Please contact your funding specialist for more information.</span>
            </div></div>
        </div>
        </div>
      }
      </Layout>
    );
  }
}

AccountLinking.propTypes = {
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  AccountLinking: makeSelectAccountLinking(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AccountLinking));
