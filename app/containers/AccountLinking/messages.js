/*
 * AccountLinking Messages
 *
 * This contains all the text for the AccountLinking component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AccountLinking.header',
    defaultMessage: 'This is AccountLinking container !',
  },
});
