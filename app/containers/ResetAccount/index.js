/*
 *
 * ResetAccount
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import { createStructuredSelector } from 'reselect';
import Layout from 'components/PublicLayout';
import authCheck from 'components/Helper/authCheck';
import AlertMessage from 'components/AlertMessage';
import { required } from 'lib/validation/required';
import { isInLength } from 'lib/validation/isInLength';
import makeSelectResetAccount from './selectors';
import messages from './messages';

export class ResetAccount extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      token: props.location.pathname.split('/')[2],
      username: props.location.pathname.split('/')[3],
      isVisible: false,
      confirmPassword: '',
      password: '',
    };
  }
  modifyJason = (values) => {
    this.setState({
      confirmPassword: values.confirmPassword,
      password: values.password,
    });
    const request = {
      ...values,
      username: this.state.username,
      token: this.state.token,
      realm: 'broker-portal',
    };
    return request;
  };
  afterSubmitHandle = (e) => {
    if (e) {
      if (e.body.passwordNotMatch) {
        this.setState({ isError: true, message: 'Password does not match the confirm password', isVisible: true });
        return;
      }
      authCheck(this.props.dispatch)(e);
      this.setState({ isError: true, message: e.body.body.message, isVisible: true });
      return;
    }
    this.setState({ isError: false, message: 'Password updated successfully', isVisible: true });
  };
  render() {
    const parsedMessages = localityNormalizer(messages);
    return (
      <Layout>
        <div className="container">
          <div className="row">
            <div className="login_wraper">
              <div className="col-md-6 col-sm-8 login-content">
                <div className="main_login">
                  <h3>Reset Password</h3>
                  <div className="main_logininner">
                    <Form initialValuesBuilder={() => ({ password: this.state.password, confirmPassword: this.state.confirmPassword })} afterSubmit={this.afterSubmitHandle} name="resetAccount" action="authorization/identity/reset-account" payloadBuilder={this.modifyJason}>
                      <TextField type="password" name="password" {...parsedMessages.password} validate={[required('New Password'), isInLength(5, 20)]} />
                      <TextField type="password" name="confirmPassword" {...parsedMessages.confirmPassword} validate={[required('Confirm Password'), isInLength(5, 20)]} />
                      <button type="submit" className="loin_btn">Submit </button>
                    </Form>
                    {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
                    <p><Link to="/login">Click here</Link> to redirect to login page</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-sm-1 col-xs-12">
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

ResetAccount.propTypes = {
  location: PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ResetAccount: makeSelectResetAccount(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ResetAccount));
