/*
 *
 * Finalization
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter, browserHistory } from 'react-router';
import { executeAPI } from 'components/Helper/execute';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import FinalApplicationDetail from 'components/FinalApplicationDetail';
import ApplicantDetail from 'components/ApplicantDetail';
import makeAuthSelector from 'sagas/auth/selectors';
import img from 'assets/images/agreement-icon.png';
import { createStructuredSelector } from 'reselect';
import makeSelectFinalization from './selectors';

export class Finalization extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.location.pathname.split('/')[2] };
  }
  componentDidMount() {
    const payLoad = {
      applicationNumber: this.state.applicationNumber,
    };
    executeAPI('api/finalization/get-finalization', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
        if (response.data.status.code === 400) {
          if (!this.props.auth.user) {
            browserHistory.replace('/login');
          } else {
            browserHistory.replace('/dashboard');
          }
        }
      } else {
        this.setState({ Finalization: response.data.body });
      }
    });
  }
  render() {
    if (!this.state || !this.state.Finalization) {
      return <Spinner />;
    }
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="4" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Thanks for submitting the agreement!</b>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="finalization-wrap">
              <div className="col-lg-10 no-pad">
                <div className="final-head">
                  <h4>{'Here\'s your deal summary:'}</h4>
                </div>
                <div className="col-lg-5 col-sm-6 ">
                  <FinalApplicationDetail applicationDetails={this.state.Finalization} />
                </div>
                <div className="col-lg-5 col-sm-6">
                  <div className="submit-agreement">
                    <div className="icon-bx">
                      <img src={img} alt="" />
                    </div>
                    <div className="agree-text">
                      <h3>What happens now?</h3>
                      <p>Your application is currently being processed for funding. Upon completion, the funds will be deposited in your client’s account.</p>
                      <a href="/dashboard">Back to Dashboard</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Finalization.propTypes = {
  location: PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  Finalization: makeSelectFinalization(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Finalization));
