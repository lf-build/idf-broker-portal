/*
 *
 * Verification
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter, browserHistory } from 'react-router';
import Layout from 'containers/AppLayout';
import { createStructuredSelector } from 'reselect';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import ApplicantDetail from 'components/ApplicantDetail';
import ClientEmailVerification from 'components/ClientEmailVerification';
import ClientAccountLinking from 'components/ClientAccountLinking';
import UnderWritingStatus from 'components/UnderWritingStatus';
import { simplePhoneMasking } from 'components/Helper/formatting';
import { executeAPI } from 'components/Helper/execute';
import makeAuthSelector from 'sagas/auth/selectors';
import infoImg from 'assets/images/info.png';
import makeSelectVerification from './selectors';

export class Verification extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      applicationNumber: this.props.location.pathname.split('/')[2],
    };
  }
  componentDidMount() {
    const payLoad = {
      applicationNumber: this.state.applicationNumber,
    };
    executeAPI('api/verification/get-verification', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
        if (response.data.status.code === 400) {
          if (!this.props.auth.user) {
            browserHistory.replace('/login');
          } else {
            browserHistory.replace('/dashboard');
          }
        }
      } else {
        this.setState({ Verification: response.data.body, applicationNumber: this.props.location.pathname.split('/')[2] });
      }
    });
  }
  render() {
    if (!this.state || !this.state.Verification) {
      return <Spinner />;
    }
    const { emailStatus, phoneNumber, bankLinkingStatus, underWritingStatus, emailBody } = this.state.Verification;
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="1" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Linking your client’s accounts</b>
                <p>Help your client get approved by having them connect their Accounting System &amp; Bank Account.</p>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="verification-wrap">
              <div className="col-lg-12 no-pad">
                <div className="col-sm-4">
                  <ClientEmailVerification emailBody={emailBody} phoneNumber={simplePhoneMasking(phoneNumber)} emailStatus={emailStatus} />
                </div>
                <div className="col-sm-4">
                  <ClientAccountLinking emailBody={emailBody} bankLinkingStatus={bankLinkingStatus} />
                </div>
                <div className="col-sm-4">
                  <UnderWritingStatus underWritingStatus={underWritingStatus} />
                </div>
              </div>
            </div>
            <div className="clearfix"></div>
            {emailStatus && bankLinkingStatus && <div className="manual-underwriting">
              <div className="col-md-10">
                <p><img alt="" src={infoImg} className="img_left" />
               Underwriting may take longer than usual due to an identity verification issue.
                  <a target="_blank" href="https://divergecap.com/partner-faq/">Partner FAQ</a></p>
              </div>
            </div>}
          </div>
        </div>
      </Layout>
    );
  }
}

Verification.propTypes = {
  location: PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  Verification: makeSelectVerification(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Verification));
