/*
 *
 * Dashboard
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { executeAPI } from 'components/Helper/execute';
import { FormattedMessage, FormattedDate, FormattedNumber } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { browserHistory, Link, withRouter } from 'react-router';
import Layout from 'containers/AppLayout';
import ApplicationMatrix from 'components/ApplicationMatrix';
import makeAuth from 'sagas/auth/selectors';
import DataGrid from '@ui/DataGrid';
import getRedirectionUrl from 'lib/getRedirectionUrl';
import messages from './messages';

const columnConfiguration = [{
  value: (row) => <Link to={getRedirectionUrl(row.StatusCode, row.ApplicationNumber, row.tags)}> {row.ApplicationNumber} </Link>,
  key: 'App ID',
  sortBy: 'ApplicationNumber',
  searchBy: 'ApplicationNumber',
}, {
  value: (row) => <FormattedDate
    value={new Date(row.Submitted.Time.DateTime)}
    year="numeric"
    month="long"
    day="2-digit"
  />,
  key: 'Submitted',
  sortBy: 'Submitted.Time.Ticks',
}, {
  value: 'BusinessApplicantName',
  key: 'Applicant',
  sortBy: 'BusinessApplicantName',
  searchBy: 'BusinessApplicantName',
}, {
  value: 'LegalBusinessName',
  key: 'Business',
  sortBy: 'LegalBusinessName',
  searchBy: 'LegalBusinessName',
}, {
  value: (row) => row.RequestedAmount ? <FormattedNumber
    value={row.RequestedAmount} minimumFractionDigits={0}
  maximumFractionDigits={0} style="currency" currency="USD" /> : '-', // eslint-disable-line
  key: '$ Requested',
  sortBy: 'RequestedAmount',
}, {
  value: (row) => row.LoanAmount ? <FormattedNumber
    minimumFractionDigits={0}
  maximumFractionDigits={0} value={row.LoanAmount} style="currency" currency="USD" /> : '-',  // eslint-disable-line
  key: '$ Offered',
  sortBy: 'LoanAmount',
}, {
  value: 'StatusName',
  key: 'Status',
  sortBy: 'StatusName',
  searchBy: 'StatusName',
}, {
  value: (row) => row.LastProgressDate ? <FormattedDate
    value={new Date(row.LastProgressDate.Time.DateTime)}
    year="numeric"
    month="long"
    day="2-digit"
  /> : '-',
  key: 'Updated',
  sortBy: 'LastProgressDate.Time.DateTime',
  default: 'true',
}];

export class Dashboard extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      filters: {
        page: 1,
      },
      items: [],
      partnerId: (this.props && this.props.auth && this.props.auth.user && this.props.auth.user.partnerId) ? this.props.auth.user.partnerId : undefined,
      filter: this.props.location.pathname.split('/')[2],
    };
  }
  componentWillMount() {
    if (!this.state.partnerId) {
      browserHistory.replace('/login');
    }
  }
  componentDidMount() {
    if (this.state.partnerId) {
      const payLoad = {
        partnerId: this.state.partnerId,
      };
      executeAPI('api/dashboard/get-dashboard-count', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
        if (response.error) {
      // add extra error handling code apart from authCheck
        } else {
          this.setState({ dashboardMatrix: response.data.body });
        }
      });
      this.applyTag(this.state.filter);
    }
  }
  handleEnterKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.state.grid.search(this.state.searchField);
    }
  }
  applyTag(tag) {
    this.setState({ tag });
    if (tag === 'active') {
      this.state.filters.raw = {
        StatusName: {
          $nin: ['Declined', 'Funded', 'Not Interested', 'Discarded'],
        },
      };
    } else if (tag === 'closed') {
      this.state.filters.raw = {
        StatusName: {
          $in: ['Declined', 'Funded', 'Not Interested'],
        },
      };
    } else {
      this.state.filters.raw = {
        StatusName: { $nin: ['Discarded'],
        },
      };
    }
    this.state.filters.raw.PartnerId = this.state.partnerId;
    this.state.grid.loadGridData(this.state.filters.raw, true);
  }
  render() {
    return (<Layout>
      <div className="containt_wraper">
        <h2><FormattedMessage {...messages.header} /></h2>
        <ApplicationMatrix dashboardMatrix={this.state.dashboardMatrix} />
        <div className="dashboard_data" >
          <div className="col-sm-12">
            <div className="status_wraper">
              <div className="row">
                <div className=" col-lg-3 col-md-3 col-sm-6 col-xs-12 no-pad ">
                  <div className="table_searchdata">
                    <div className="input-group search-d">
                      <input onKeyPress={(e) => this.handleEnterKeyPress(e)} onChange={(e) => this.setState({ searchField: e.target.value })} type="text" className="form-control" aria-describedby="basic-addon2" />
                      <span className="input-group-addon"> <button type="button" onClick={() => this.state.grid.search(this.state.searchField)}>Search</button> </span>
                    </div>
                  </div>
                </div>
                <div className=" col-lg-3 col-md-3  hidden-sm col-xs-12"></div>
                <div className=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
                  <Link onClick={() => this.applyTag()} className={!this.state.tag ? 'active active-btn' : 'deactive-btn'}>All</Link>
                </div>
                <div className=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
                  <Link onClick={() => this.applyTag('active')} className={this.state.tag === 'active' ? 'active active-btn' : 'deactive-btn'}>Active</Link>
                </div>
                <div className=" col-lg-2 col-md-2 col-sm-2 col-xs-12">
                  <Link onClick={() => this.applyTag('closed')} className={this.state.tag === 'closed' ? 'active active-btn' : 'deactive-btn'}>Closed</Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-sm-12">
            <DataGrid getRef={(grid) => { this.state.grid = grid; }} onRowClicked={(row) => browserHistory.replace(getRedirectionUrl(row.StatusCode, row.ApplicationNumber, row.tags))} columns={columnConfiguration} items="api/dashboard/get-dashboard-grid" noRecordsMessage="No Applications Found" paginationLabel="Showing applications" />
          </div>
        </div>
      </div>
    </Layout>);
  }
}

Dashboard.propTypes = {
  auth: React.PropTypes.object,
  location: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuth(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Dashboard));
