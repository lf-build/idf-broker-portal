import { takeEvery, cancel, take } from 'redux-saga/effects';
import { browserHistory } from 'react-router';

function* redirectToPage() {
  const url = browserHistory.getCurrentLocation();
  if (url && url.query && url.query.return_url) {
    browserHistory.replace(url.query.return_url);
    return;
  }
  browserHistory.replace('/dashboard');
}

// Individual exports for testing
export function* defaultSaga() {
  const watcher = yield takeEvery('app/uplink/EXECUTE_UPLINK_REQUEST_ENDED_who-am-i', redirectToPage);
  yield take('@@router/LOCATION_CHANGE');
  yield cancel(watcher);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
