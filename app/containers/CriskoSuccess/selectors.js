import { createSelector } from 'reselect';

/**
 * Direct selector to the criskoSuccess state domain
 */
const selectCriskoSuccessDomain = () => (state) => state.get('criskoSuccess');

/**
 * Other specific selectors
 */


/**
 * Default selector used by CriskoSuccess
 */

const makeSelectCriskoSuccess = () => createSelector(
  selectCriskoSuccessDomain(),
  (substate) => substate.toJS()
);

export default makeSelectCriskoSuccess;
export {
  selectCriskoSuccessDomain,
};
