/*
 *
 * CriskoSuccess
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router';
import { executeAPI } from 'components/Helper/execute';
import makeSelectCriskoSuccess from './selectors';


export class CriskoSuccess extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { businessId: this.props.location.pathname.split('/')[3] };
  }
  componentDidMount() {
    this.loadData();
  }
  loadData() {
    const payLoad = {
      applicationNumber: window.opener.applicationNumber,
      businessId: this.state.businessId,
    };
    executeAPI('api/account-linking/set-crisko-id', payLoad, undefined, undefined, false, (response) => {
      if (response.error) {
      // error handling
      } else {
        window.close();
      }
    });
  }
  render() {
    return (
      <div>
      </div>
    );
  }
}

CriskoSuccess.propTypes = {
  location: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  CriskoSuccess: makeSelectCriskoSuccess(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CriskoSuccess));
