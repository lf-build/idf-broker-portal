/*
 * CriskoSuccess Messages
 *
 * This contains all the text for the CriskoSuccess component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.CriskoSuccess.header',
    defaultMessage: 'This is CriskoSuccess container !',
  },
});
