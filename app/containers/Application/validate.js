import isInRange from 'lib/validation/isInRange';
import { isInLength } from 'lib/validation/isInLength';
import { isSSN } from 'lib/validation/isSSN';
import { isMobile } from 'lib/validation/isMobile';
import { isPostalCode } from 'lib/validation/isPostalCode';
import isRegexMatch from 'lib/validation/isRegexMatch';
import { emailRegex, mobileRegex, ssnRegex, postalCodeRegex, dateRegex } from 'lib/validation/regexList';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();
  values.draft = that.saveAsDraft;
  const minMax2To100 = isInLength(2, 100);
  const ssnCheck = isSSN('SSN');
  const primaryMobileCheck = isMobile('Primary Phone');
  const secondaryMobileCheck = isMobile('Secondary Phone');
  const zipCheck = isPostalCode('Zip Code');

  // Save draft validations
  if (values.draft) {
    if (!values.owners || !values.owners.length) {
      errors.owners = { _error: 'At least one owner required' };
    } else {
      const ownerErrorsArray = [];
    values.owners.map((owner, index) => { //eslint-disable-line
      const ownerErrors = {};
      if (owner) {
        ownerErrors.firstName = owner.firstName ? minMax2To100(owner.firstName) : undefined;
        ownerErrors.lastName = owner.lastName ? minMax2To100(owner.lastName) : undefined;
        ownerErrors.city = owner.city ? minMax2To100(owner.city) : undefined;
        ownerErrors.homeAddress = owner.homeAddress ? minMax2To100(owner.homeAddress) : undefined;
        if (!owner || !owner.emailAddress) {
          ownerErrors.emailAddress = 'Email Address is required';
        } else if (!isRegexMatch(owner.emailAddress, emailRegex)) {
          ownerErrors.emailAddress = 'Please enter a valid email';
        }
        ownerErrors.ssn = ssnCheck(owner.ssn);
        ownerErrors.primaryPhone = owner.primaryPhone ? primaryMobileCheck(owner.primaryPhone) : undefined;
        ownerErrors.secondaryPhone = owner.secondaryPhone ? secondaryMobileCheck(owner.secondaryPhone) : undefined;
        ownerErrors.zipCode = zipCheck(owner.zipCode);
      }
      ownerErrorsArray[index] = ownerErrors;
    });
      if (ownerErrorsArray.length) {
        errors.owners = ownerErrorsArray;
      }
    }
    if (values.businessLegalName) {
      errors.businessLegalName = values.businessLegalName ? minMax2To100(values.businessLegalName) : undefined;
    }
    if (values.dba) {
      errors.dba = values.dba ? minMax2To100(values.dba) : undefined;
    }
    if (values.ein) {
      if (!isInRange(values.ein.replace('-', ''), 9, 9)) {
        errors.ein = 'Please enter at least 9 characters';
      }
    }
    if (values.businessAddress) {
      errors.businessAddress = values.businessAddress ? minMax2To100(values.businessAddress) : undefined;
    }
    if (values.city) {
      errors.city = values.city ? minMax2To100(values.city) : undefined;
    }
    if (values.zipCode) {
      if (!isRegexMatch(values.zipCode, postalCodeRegex)) {
        errors.zipCode = 'Please enter a valid zip code';
      }
    }
    if (values.officePhone) {
      if (!isRegexMatch(values.officePhone.replace(/ /g, ''), mobileRegex)) {
        errors.officePhone = 'Please enter a valid phone number';
      }
    }
  } else {
  // Next button validations
    if (!values.owners || !values.owners.length) {
      errors.owners = { _error: 'At least one owner required' };
    } else {
      const ownerErrorsArray = [];
    values.owners.map((owner, index) => { //eslint-disable-line
      const ownerErrors = {};
      if (!owner || !owner.firstName) {
        ownerErrors.firstName = 'First Name is required';
      } else if (!isInRange(owner.firstName, 2, 100)) {
        ownerErrors.firstName = 'Please enter at least 2 characters';
      }
      if (!owner || !owner.lastName) {
        ownerErrors.lastName = 'Last Name is required';
      } else if (!isInRange(owner.lastName, 2, 100)) {
        ownerErrors.lastName = 'Please enter at least 2 characters';
      }
      if (!owner || !owner.ssn) {
        ownerErrors.ssn = 'SSN is required';
      } else if (!isRegexMatch(owner.ssn, ssnRegex)) {
        ownerErrors.ssn = 'Please enter a valid SSN';
      }
      if (!owner || !owner.primaryPhone) {
        ownerErrors.primaryPhone = 'Primary Phone is required';
      } else if (!isRegexMatch(owner.primaryPhone.replace(/ /g, ''), mobileRegex)) {
        ownerErrors.primaryPhone = 'Please enter a valid phone number';
      }
      if ((owner && owner.secondaryPhone)) {
        if (!isRegexMatch(owner.secondaryPhone.replace(/ /g, ''), mobileRegex)) {
          ownerErrors.secondaryPhone = 'Please enter a valid phone number';
        }
      }
      if (!owner || !owner.emailAddress) {
        ownerErrors.emailAddress = 'Email Address is required';
      } else if (!isRegexMatch(owner.emailAddress, emailRegex)) {
        ownerErrors.emailAddress = 'Please enter a valid email';
      }
      if (!owner || !owner.homeAddress) {
        ownerErrors.homeAddress = 'Home Address is required';
      } else if (!isInRange(owner.homeAddress, 2, 100)) {
        ownerErrors.homeAddress = 'Please enter at least 2 characters';
      }
      if (!owner || !owner.city) {
        ownerErrors.city = 'City is required';
      } else if (!isInRange(owner.city, 2, 100)) {
        ownerErrors.city = 'Please enter at least 2 characters';
      }
      if (!owner || !owner.state) {
        ownerErrors.state = 'State is required';
      }
      if (!owner || !owner.zipCode) {
        ownerErrors.zipCode = 'Zip Code is required';
      } else if (!isRegexMatch(owner.zipCode, postalCodeRegex)) {
        ownerErrors.zipCode = 'Please enter a valid zip Code';
      }
      if (!owner || !owner.dateOfBirth) {
        ownerErrors.dateOfBirth = 'Date of birth is required';
      } else if (!isRegexMatch(owner.dateOfBirth, dateRegex)) {
        ownerErrors.dateOfBirth = 'Please enter a valid date';
      }
      ownerErrorsArray[index] = ownerErrors;
    });
      if (ownerErrorsArray.length) {
        errors.owners = ownerErrorsArray;
      }
    }
    if (!values.businessLegalName) {
      errors.businessLegalName = 'Business Legal Name is required';
    } else if (!isInRange(values.businessLegalName, 2, 100)) {
      errors.businessLegalName = 'Please enter at least 2 characters';
    }
    if (!values.dba) {
      errors.dba = 'DBA is required';
    } else if (!isInRange(values.dba, 2, 100)) {
      errors.dba = 'Please enter at least 2 characters';
    }
    if (!values.ein) {
      errors.ein = 'EIN is required';
    } else if (!isInRange(values.ein.replace('-', ''), 9, 9)) {
      errors.ein = 'Please enter at least 9 characters';
    }
    if (!values.businessAddress) {
      errors.businessAddress = 'Business Address is required';
    } else if (!isInRange(values.businessAddress, 2, 100)) {
      errors.businessAddress = 'Please enter at least 2 characters';
    }
    if (!values.city) {
      errors.city = 'City is required';
    } else if (!isInRange(values.city, 2, 100)) {
      errors.city = 'Please enter at least 2 characters';
    }
    if (!values.state) {
      errors.state = 'State is required';
    }
    if (!values.zipCode) {
      errors.zipCode = 'Zip Code is required';
    } else if (!isRegexMatch(values.zipCode, postalCodeRegex)) {
      errors.zipCode = 'Please enter a valid zip Code';
    }
    if (!values.officePhone) {
      errors.officePhone = 'Office Phone is required';
    } else if (!isRegexMatch(values.officePhone.replace(/ /g, ''), mobileRegex)) {
      errors.officePhone = 'Please enter a valid phone number';
    }
    if (!values.requestedAmount) {
      errors.requestedAmount = 'Requested Amount is required';
    } else if (!isInRange(isNaN(values.requestedAmount) ? parseInt(values.requestedAmount.replace(/[$, ]/g, ''), 10) : values.requestedAmount, 5000, 100000)) {
      errors.requestedAmount = 'We currently accept requests between $5,000 and $100,000';
    }
    if (!values.consents) {
      errors.consents = 'Please check consent checkbox';
    }
  }
  return errors;
};

export default validateForm;
