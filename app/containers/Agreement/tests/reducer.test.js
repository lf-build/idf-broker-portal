
import { fromJS } from 'immutable';
import agreementReducer from '../reducer';

describe('agreementReducer', () => {
  it('returns the initial state', () => {
    expect(agreementReducer(undefined, {})).toEqual(fromJS({}));
  });
});
