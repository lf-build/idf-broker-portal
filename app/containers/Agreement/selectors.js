import { createSelector } from 'reselect';

/**
 * Direct selector to the agreement state domain
 */
const selectAgreementDomain = () => (state) => state.get('agreement');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Agreement
 */

const makeSelectAgreement = () => createSelector(
  selectAgreementDomain(),
  (substate) => substate.toJS()
);

export default makeSelectAgreement;
export {
  selectAgreementDomain,
};
