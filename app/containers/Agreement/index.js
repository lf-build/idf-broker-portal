/*
 *
 * Agreement
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory, withRouter, Link } from 'react-router';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import ApplicantDetail from 'components/ApplicantDetail';
import DocuSignResend from 'components/DocuSignResend';
import Stipulation from 'components/Stipulation';
import AlertMessage from 'components/AlertMessage';
import { executeAPI } from 'components/Helper/execute';
import { createStructuredSelector } from 'reselect';
import makeSelectAgreement from './selectors';

export class Agreement extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.location.pathname.split('/')[2], isVisible: false };
  }
  componentDidMount() {
    this.loadData();
  }
  loadData() {
    const payLoad = {
      applicationNumber: this.state.applicationNumber,
    };
    executeAPI('api/agreement/get-agreement', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
      // add extra error handling code apart from authCheck
      } else {
        this.setState({ Agreement: response.data.body });
      }
    });
  }
  notInterested() {
    const payLoad = {
      applicationNumber: this.state.applicationNumber,
      rejectCode: '700.20',
      reason: 'Clientnotinterested',
    };
    executeAPI('api/application/discard-application', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
        this.setState({ isError: true, message: 'Something went wrong. Please try again later.', isVisible: true });
      } else {
        browserHistory.replace('/dashboard');
      }
    });
  }

  render() {
    if (!this.state || !this.state.Agreement) {
      return <Spinner />;
    }
    const { contract, photoId, voidedCheck, taxReturn, lien, confession, finalizationStatus } = this.state.Agreement;
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="3" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Agreement Generated </b>
                <p> Please fill out the steps below to complete the process.</p>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="col-lg-10">
              <div className="agree-head">
                <h3>Contract</h3>
              </div>
              <div className="agreement-box">
                <div className="col-sm-4">
                  <div className="receive-box">
                    <h3>{'Receive client\'s signature'}</h3>
                    <p>Securely link your bank accounts to give our team a view of your finances.</p>
                  </div>
                </div>
                <DocuSignResend email={contract.email} applicationNumber={this.state.applicationNumber} docuSignStatus={contract.docuSignStatus} />
                <Stipulation subHeader={'Client signed on printed document?'} Stipulation={contract} uploadFileType={'signeddocument'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="agree-details">
              <Stipulation mainHeader={'Voided Check'} subHeader={'Upload a Voided Check image'} documentNameList={'Documents can be uploaded in PDF, PNG or JPEG format'} Stipulation={voidedCheck} uploadFileType={'voidedcheck'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />
              <Stipulation mainHeader={'Photo ID'} subHeader={'Please Upload a Copy of Your Photo ID'} documentNameList={'Submit Drivers License,State ID or Passport'} Stipulation={photoId} uploadFileType={'photoid'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />
              {taxReturn.isDisplay && <Stipulation mainHeader={'Tax Return'} subHeader={'Please Upload a copy of Tax Return'} documentNameList={'Documents can be uploaded in PDF, PNG or JPEG format'} Stipulation={taxReturn} uploadFileType={'taxreturnstatement'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />}
              {confession.isDisplay && <Stipulation mainHeader={'Confession'} subHeader={'Please Upload a copy of Confession'} documentNameList={'Documents can be uploaded in PDF, PNG or JPEG format'} Stipulation={confession} uploadFileType={'confessionofjudgment'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />}
              {lien.isDisplay && <Stipulation mainHeader={'Lien'} subHeader={'Please Upload a copy of Lien'} documentNameList={'Documents can be uploaded in PDF, PNG or JPEG format'} Stipulation={lien} uploadFileType={'liendocument'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />}
            </div>
            <div className="button-boxs">
              {finalizationStatus && <button className="button-b" onClick={() => browserHistory.replace(`/finalization/${this.state.applicationNumber}`)}>Finish</button>}
              {!finalizationStatus && <button className="button-b-disable" disabled>Finish</button>}
              <br /><Link onClick={() => this.notInterested()}>Client is not Interested</Link>
            </div>
            <div className="col-lg-4">
              {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Agreement.propTypes = {
  location: PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Agreement: makeSelectAgreement(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Agreement));
