
import { fromJS } from 'immutable';
import pricingReducer from '../reducer';

describe('pricingReducer', () => {
  it('returns the initial state', () => {
    expect(pricingReducer(undefined, {})).toEqual(fromJS({}));
  });
});
