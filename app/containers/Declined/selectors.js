import { createSelector } from 'reselect';

/**
 * Direct selector to the declined state domain
 */
const selectDeclinedDomain = () => (state) => state.get('declined');

/**
 * Other specific selectors
 */


/**
 * Default selector used by Declined
 */

const makeSelectDeclined = () => createSelector(
  selectDeclinedDomain(),
  (substate) => substate.toJS()
);

export default makeSelectDeclined;
export {
  selectDeclinedDomain,
};
