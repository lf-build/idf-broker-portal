/*
 *
 * Declined
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { withRouter, browserHistory } from 'react-router';
import { executeAPI } from 'components/Helper/execute';
import Layout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import Breadcrumb from 'components/Breadcrumb';
import FinalApplicationDetail from 'components/FinalApplicationDetail';
import ApplicantDetail from 'components/ApplicantDetail';
import makeAuthSelector from 'sagas/auth/selectors';
import img from 'assets/images/error-icon.png';
import { createStructuredSelector } from 'reselect';
import makeSelectDeclined from './selectors';

export class Declined extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.location.pathname.split('/')[2] };
  }
  componentDidMount() {
    const payLoad = {
      applicationNumber: this.state.applicationNumber,
    };
    executeAPI('api/declined/get-declined', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
        if (response.data.status.code === 400) {
          if (!this.props.auth.user) {
            browserHistory.replace('/login');
          } else {
            browserHistory.replace('/dashboard');
          }
        }
      } else {
        this.setState({ declineReasons: response.data.body.declineReasons, applicationDetails: response.data.body.applicationDetails });
      }
    });
  }
  render() {
    if (!this.state || !this.state.declineReasons) {
      return <Spinner />;
    }
    return (
      <Layout>
        <div className="application_wraper">
          <div className="container-fluid responsive-wrap">
            <ApplicantDetail />
            <div className="clearfix"></div>
            <Breadcrumb applicationStatus="4" />
            <div className="clearfix"></div>
            <div className="col-xs-12">
              <div className="header_caption">
                <b>Application disqualified</b>
                <p>Unfortunately, your application was not approved for funding at this time. <br />You may resubmit the application should there be a positive change in status for the client.</p>
              </div>
            </div>
            <div className="clearfix"></div>
            <div className="finalization-wrap">
              <div className="col-lg-10 no-pad">
                <div className="final-head">
                  <h4>{'Here\'s your deal summary:'}</h4>
                </div>
                <div className="col-lg-5 col-sm-6 ">
                  <FinalApplicationDetail applicationDetails={this.state.applicationDetails} />
                </div>
                <div className="col-lg-5 col-sm-6">
                  <div className="dis-agreement">
                    <div className="icon-err">
                      <img src={img} alt="" />
                    </div>
                    <div className="disq-text">
                      <h3>Application disqualified for the following reasons:</h3>
                      <ul className="error-msgs">
                        {this.state.declineReasons.map((declineReason, index) => (
                          <li key={index}>
                            <h5>{declineReason.reason}</h5>
                            <p>{declineReason.subReason}</p>
                          </li>
                      ))}
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

Declined.propTypes = {
  location: PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  Declined: makeSelectDeclined(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Declined));
