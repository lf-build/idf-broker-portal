/*
 *
 * AppLayout
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Header from 'components/Header';
import Sidenav from 'components/Sidenav';

export class AppLayout extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: React.PropTypes.node,
    // dispatch: PropTypes.func.isRequired,
  };
  render() {
    return (
      <div style={{ height: '100%', position: 'absolute', width: '100%' }}>
        <Header />
        <div className="contain-wrapper">
          <Sidenav />
          <div className="right-application col-lg-10 col-md-9">
            {React.Children.toArray(this.props.children)}
          </div>
        </div>
      </div>
    );
  }
}


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(null, mapDispatchToProps)(AppLayout);
