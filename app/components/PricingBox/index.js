/**
*
* PricingBox
*
*/

import React from 'react';
// import styled from 'styled-components';

class PricingBox extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { divergentGrade, amountFunded, amountPayback, commission, commissionPercentage } = this.props;
    let gradeDiscription = '';
    if (divergentGrade === 'A') {
      gradeDiscription = 'This client is eligible for our best terms.';
    } else if (divergentGrade === 'B') {
      gradeDiscription = 'This client is eligible for better-than-average terms.';
    } else if (divergentGrade === 'C') {
      gradeDiscription = 'This client is eligible for funding.';
    } else if (divergentGrade === 'D') {
      gradeDiscription = 'Our ability to fund this client is limited.';
    }
    return (
      <div className="pricing_stagebox">
        <div className="row">
          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <h4 className="price-head">Divergent Grade</h4>
            <div className="pricing_stagebox_left remove_border">
              <h1 className="price-letter">{divergentGrade}</h1>
              <p className="price-txt">{gradeDiscription}</p>
            </div>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div className="pricing_stagebox_right">
              <ul>
                <li><span className="left_text active">Amount Funded</span><span className="right_text active">{`$${amountFunded}`}</span></li>
                <li><span className="left_text">Payback Amount</span><span className="right_text">{ `$${amountPayback}`}</span></li>
                <li><span className="left_text">Commission($)</span> <span className="right_text">{ `$${commission}`}</span></li>
                <li><span className="left_text">Commission(%)</span> <span className="right_text">{ `${commissionPercentage}%`}</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PricingBox.propTypes = {
  divergentGrade: React.PropTypes.any,
  amountFunded: React.PropTypes.any,
  amountPayback: React.PropTypes.any,
  commission: React.PropTypes.any,
  commissionPercentage: React.PropTypes.any,
};

export default PricingBox;
