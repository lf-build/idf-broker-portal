/*
 * ApplicantDetail Messages
 *
 * This contains all the text for the ApplicantDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ApplicantDetail.header',
    defaultMessage: 'This is the ApplicantDetail component !',
  },
});
