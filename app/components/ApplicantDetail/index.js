/**
*
* ApplicantDetail
*
*/

import React from 'react';
import { browserHistory, withRouter } from 'react-router';
import { connect } from 'react-redux';
import authCheck from 'components/Helper/authCheck';
import { executeAPI } from 'components/Helper/execute';
import { createStructuredSelector } from 'reselect';
import { phoneMasking } from 'components/Helper/formatting';
import makeAuthSelector from 'sagas/auth/selectors';
import getRedirectionUrl from 'lib/getRedirectionUrl';

class ApplicantDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { partnerId: (this.props.auth && this.props.auth.user && this.props.auth.user.partnerId) ? this.props.auth.user.partnerId : undefined };
  }
  componentDidMount() {
    const payLoad = { applicationNumber: this.props.location.pathname.split('/')[2] };
    try {
      executeAPI('api/application/applicant-details', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
        if (response.error) {
          if (response.data.status.code === 400) {
            if (!this.props.auth.user) {
              browserHistory.replace('/login');
            } else {
              browserHistory.replace('/dashboard');
            }
          }
        } else {
          // Try to access page without log in, redirect to login page with return_url
          if (!this.state.partnerId) {
            throw { status : { code: 401 }}; // eslint-disable-line
          }

          // if accessing other broker's application, redirect to dashboard page
          if (this.props.auth.user.partnerId !== response.data.body.partnerId) {
            browserHistory.replace('/dashboard');
            return;
          }

          // if manually accessing application's stage in which it is not, than redirects to actual stage page
          const urlTo = getRedirectionUrl(response.data.body.code, response.data.body.applicationNumber, response.data.body.tags);
          const currentUrl = `${this.props.location.pathname.split('/')[1]}/${this.props.location.pathname.split('/')[2]}`;
          if (currentUrl !== urlTo) {
            browserHistory.replace(urlTo);
          }
          this.setState({ legalBusinessName: response.data.body.legalBusinessName,
            applicationNumber: response.data.body.applicationNumber,
            applicantName: response.data.body.applicantName,
            email: response.data.body.email,
            phoneNumber: phoneMasking(response.data.body.phoneNumber),
          });
        }
      });
    } catch (e) {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
    }
  }
  render() {
    return (
      <div className="col-xs-12">
        <h2 className="status-heading">{this.state.legalBusinessName}</h2>
        <ul className="header_staus">
          <li><a>Application {this.state.applicationNumber}</a></li>
          <li><a>{this.state.applicantName}</a></li>
          <li><a>{this.state.email}</a></li>
          <li><a className="remove_rightborder">{this.state.phoneNumber}</a></li>
        </ul>
      </div>
    );
  }
}

ApplicantDetail.propTypes = {
  location: React.PropTypes.object.isRequired,
  auth: React.PropTypes.object,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ApplicantDetail));
