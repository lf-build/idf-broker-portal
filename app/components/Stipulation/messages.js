/*
 * Stipulation Messages
 *
 * This contains all the text for the Stipulation component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Stipulation.header',
    defaultMessage: 'This is the Stipulation component !',
  },
});
