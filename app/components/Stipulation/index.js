/**
*
* Stipulation
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import FileDropZone from 'components/FileDropZone';
import AlertMessage from 'components/AlertMessage';
import Spinner from 'components/Spinner';
import { executeAPI } from 'components/Helper/execute';
import { IsUnderFileSizeLimit, IsValidFileFormat } from 'components/Helper/functions';

class Stipulation extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, showSpinner: props.showSpinner };
  }
  render() {
    const { files } = this.props.Stipulation;
    const { applicationNumber, uploadFileType, documentNameList, subHeader, mainHeader, onDropSuccess } = this.props;
    const OnDropDone = (acceptedFiles) => {
      if (acceptedFiles[0].length > 1) {
        this.setState({ isError: true, message: 'You may upload 1 file at a time.', isVisible: true });
        return;
      }
      if (!IsValidFileFormat(acceptedFiles[0])) {
        this.setState({ isError: true, message: 'Please upload a PDF, JPG, or PNG file.', isVisible: true });
        return;
      }
      if (!IsUnderFileSizeLimit(acceptedFiles[0])) {
        this.setState({ isError: true, message: 'The maximum file size is 5MB', isVisible: true });
        return;
      }
      const newFileObject = [];
      acceptedFiles[0].forEach((ele) => {
        newFileObject.push({ dataUrl: ele.dataUrl, file: ele.file.toLowerCase() });
      }, this);
      const payLoad = {
        applicationNumber,
        uploadFileType,
        fileList: newFileObject,
      };
      executeAPI('api/agreement/upload-engine', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
        if (response.error) {
        // add extra error handling code apart from authCheck
        } else {
          this.setState({ isVisible: false, showSpinner: true });
          onDropSuccess();
          setTimeout(() => { this.setState({ showSpinner: false }); }, 1500);
        }
      });
    };
    if (uploadFileType === 'signeddocument') {
      return (
        <div className="col-sm-4">
          <div className="contract-box">
            <div className="doc-txt">
              <p>{subHeader}</p>
            </div>
            <FileDropZone files={files} applicationNumber={applicationNumber} onDrop={OnDropDone} onDiscard={() => onDropSuccess()} uploadFileType={uploadFileType} />
            {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
            { this.state.showSpinner && <Spinner /> }
          </div>
        </div>
      );
    }
    return (
      <div className="agree-wrap">
        <div className="agree-head">
          <h3>{mainHeader}</h3>
        </div>
        <div className="check-id">
          <div className="up-head">
            <h5>{subHeader}</h5>
          </div>
          <div className="up-text">
            <p>{documentNameList}</p>
          </div>
          <FileDropZone files={files} applicationNumber={applicationNumber} onDrop={OnDropDone} onDiscard={() => onDropSuccess()} uploadFileType={uploadFileType} />
          { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
          { this.state.showSpinner && <Spinner /> }
        </div>
      </div>
    );
  }
}

Stipulation.propTypes = {
  Stipulation: React.PropTypes.any,
  applicationNumber: React.PropTypes.string,
  onDropSuccess: React.PropTypes.func.isRequired,
  uploadFileType: React.PropTypes.string.isRequired,
  mainHeader: React.PropTypes.string,
  subHeader: React.PropTypes.string,
  documentNameList: React.PropTypes.string,
  showSpinner: React.PropTypes.bool,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(withRouter(Stipulation));
