/*
 * ClientEmailVerification Messages
 *
 * This contains all the text for the ClientEmailVerification component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.ClientEmailVerification.header',
    defaultMessage: 'This is the ClientEmailVerification component !',
  },
});
