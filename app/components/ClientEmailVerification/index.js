/**
*
* ClientEmailVerification
*
*/

import React from 'react';
import numberIcon from 'assets/images/number-icon.png';
import emailIcon from 'assets/images/email-icon.png';

class ClientEmailVerification extends React.Component { // eslint-disable-line react/prefer-stateless-function
  viewEmail=() => {
    const html = (this.props.emailBody && this.props.emailBody.length > 0) ?
    this.props.emailBody[this.props.emailBody.length - 1].body : 'No content';
    const newWindow = window.open();
    newWindow.document.write(html);
  }
  render() {
    const { phoneNumber, emailStatus } = this.props;
    let activeClass = 'verify-box active';
    if (!emailStatus) {
      activeClass = 'verify-box';
    }
    return (
      <div className={activeClass}>
        <div className="number-box">
          <span>1</span>
        </div>
        <div className="verify-head">
          <h4>We’ve sent your client an email on your behalf.</h4>
        </div>
        <div className="verify-img">
          <button type="button" onClick={this.viewEmail} ><img src={emailIcon} alt="" /><a>View Email</a></button>
        </div>
        <div className="verify-detail">
          <p>Bring it to their attention to speed up the process</p>
          <div className="v-number">
            <img src={numberIcon} alt="" />
            <span>{phoneNumber}</span>
          </div>
        </div>

      </div>
    );
  }
}

ClientEmailVerification.propTypes = {
  phoneNumber: React.PropTypes.string,
  emailStatus: React.PropTypes.bool,
  emailBody: React.PropTypes.any,
};

export default ClientEmailVerification;
