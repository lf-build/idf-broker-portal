/**
*
* DocuSignResend
*
*/

import React from 'react';
import { Link, withRouter } from 'react-router';
import { connect } from 'react-redux';
import Spinner from 'components/Spinner';
import { executeAPI } from 'components/Helper/execute';
import docuSignImage from 'assets/images/docuSign01.png';
import checkedImg from 'assets/images/checked.png';


class DocuSignResend extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { resendText: 'Resend' };
  }
  resendDocuSignMail(applicationNumber) {
    this.setState({ resendText: 'Resending...' });
    const payLoad = { applicationNumber };
    executeAPI('api/agreement/resend-docusign', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
        this.setState({ resendText: 'Resend Failed' });
      } else {
        this.setState({ resendText: 'Resent' });
      }
    });
  }
  render() {
    const { email, docuSignStatus, applicationNumber } = this.props;
    return (
      <div className="col-sm-4">
        <div className="contract-box">
          <div className="legal-frm">
            <p>The legal forms were sent to:</p>
            <p>{email}</p>
          </div>
          <div className="doc-status">
            <div className="doc-img">
              <img src={docuSignImage} alt="" />
            </div>
            <div className="status-d">
              <span>Status:</span>
              <h4>&nbsp;{docuSignStatus}</h4>
            </div>
          </div>
          <div className="doc-txt">
            A copy has been sent to your inbox. <Link onClick={() => this.resendDocuSignMail(applicationNumber)}>{`${this.state.resendText} `}</Link>
            { this.state.resendText === 'Resent' && <span className="right-click active"><img src={checkedImg} alt="check" /></span>}
            { this.state.resendText === 'Resending...' && <Spinner isSmall /> }
          </div>
        </div>
      </div>
    );
  }
}

DocuSignResend.propTypes = {
  email: React.PropTypes.string,
  docuSignStatus: React.PropTypes.string,
  applicationNumber: React.PropTypes.string,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(withRouter(DocuSignResend));
