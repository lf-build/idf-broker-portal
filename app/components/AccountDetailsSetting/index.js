/**
*
* AccountDetailsSetting
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import Spinner from 'components/Spinner';
import AlertMessage from 'components/AlertMessage';
import authCheck from 'components/Helper/authCheck';
import { isEmail } from 'lib/validation/isEmail';
import { required } from 'lib/validation/required';
import messages from './messages';
import makeAuthSelector from '../../sagas/auth/selectors';

const modifyJason = (values) => {
  const request = {
    ...values,
    realm: 'broker-portal',
  };
  return request;
};
class AccountDetailsSetting extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, email: (this.props.auth && this.props.auth.user && this.props.auth.user.email) ? this.props.auth.user.email : undefined };
  }
  afterSubmitHandle = (e) => {
    if (e) {
      this.setState({ isError: true, message: 'Error sending Email', isVisible: true });
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      return;
    }
    this.setState({ isError: false, message: `A password reset email has been sent to ${this.state.email}`, isVisible: true });
  };
  handleOnLoadError = (e) => {
    authCheck(this.props.dispatch)(e)(this.props.location.pathname);
  };
  render() {
    if (!this.state) {
      return <Spinner />;
    }
    const parsedMessages = localityNormalizer(messages);
    return (
      <div className="application_wraper">
        <div className="container-fluid responsive-wrap">
          <div className=" col-sm-12">
            <h2>Settings</h2>
          </div>
          <div className="res-margin">
            <div className="comman_applicationinner">
              <div className="col-md-5 col-sm-6">
                <Form afterSubmit={this.afterSubmitHandle} initialValuesBuilder={() => ({ username: this.state.email })} payloadBuilder={modifyJason} name="accountDetails" action="authorization/identity/password-reset-init" onLoadError={this.handleOnLoadError}>
                  <TextField name="username" {...parsedMessages.email} validate={[required('Email'), isEmail('Email')]} disabled />
                  <button type="submit" className="btn next_btn">RESET PASSWORD</button>
                </Form>
                {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AccountDetailsSetting.propTypes = {
  auth: React.PropTypes.object,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AccountDetailsSetting));
