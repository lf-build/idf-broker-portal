/*
 * BusinessDetails Messages
 *
 * This contains all the text for the BusinessDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.BusinessDetails.header',
    defaultMessage: 'This is the BusinessDetails component !',
  },
  businessLegalNameLabel: {
    id: 'app.components.BusinessDetails.businessLegalName',
    defaultMessage: 'Business Legal Name',
  },
  businessLegalNamePlaceholder: {
    id: 'app.components.BusinessDetails.businessLegalName',
    defaultMessage: 'Business Legal Name',
  },
  dbaLabel: {
    id: 'app.components.BusinessDetails.dba',
    defaultMessage: 'DBA',
  },
  dbaPlaceholder: {
    id: 'app.components.BusinessDetails.dba',
    defaultMessage: 'DBA',
  },
  einLabel: {
    id: 'app.components.BusinessDetails.ein',
    defaultMessage: 'EIN',
  },
  einPlaceholder: {
    id: 'app.components.BusinessDetails.ein',
    defaultMessage: 'EIN',
  },
  businessAddressLabel: {
    id: 'app.components.BusinessDetails.businessAddress',
    defaultMessage: 'Business Address',
  },
  businessAddressPlaceholder: {
    id: 'app.components.BusinessDetails.businessAddress',
    defaultMessage: 'Business Address',
  },
  cityLabel: {
    id: 'app.components.BusinessDetails.city',
    defaultMessage: 'City',
  },
  cityPlaceholder: {
    id: 'app.components.BusinessDetails.city',
    defaultMessage: 'City',
  },
  stateLabel: {
    id: 'app.components.BusinessDetails.state',
    defaultMessage: 'State',
  },
  zipCodeLabel: {
    id: 'app.components.BusinessDetails.zipCode',
    defaultMessage: 'Zip Code',
  },
  zipCodePlaceholder: {
    id: 'app.components.BusinessDetails.zipCode',
    defaultMessage: 'Zip Code',
  },
  officePhoneLabel: {
    id: 'app.components.BusinessDetails.officePhone',
    defaultMessage: 'Office Phone',
  },
  officePhonePlaceholder: {
    id: 'app.components.BusinessDetails.officePhone',
    defaultMessage: 'Office Phone #',
  },
});
