/**
*
* ApplicationMatrixLine
*
*/

import React from 'react';
import ApplicationMatrixCount from 'components/ApplicationMatrixCount';
import ApplicationMatrixCommissionSummary from 'components/ApplicationMatrixCommissionSummary';

class ApplicationMatrixLine extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { matrix, short, commission, title } = this.props;
    return (
      <div className="row">
        <div className="col-lg-8 col-md-8 col-sm-12">
          <h3 className={short ? 'youelife_section' : ''}> {title} </h3>
          <div>
            { matrix.map((p, index) => (<div key={index} className="col-sm-4">
              <ApplicationMatrixCount {...p} short={short} />
            </div>
              )) }
          </div>
        </div>
        <div className="col-lg-4 col-md-4 col-sm-12">
          <div className="col-lg-3 col-md-1 col-sm-12"></div>
          <div className="col-lg-8 col-md-10 col-sm-4">
            <ApplicationMatrixCommissionSummary short={short} commission={commission} />
          </div>
        </div>
      </div>
    );
  }
}
ApplicationMatrixLine.propTypes = {
  commission: 0,
  matrix: [],
};

ApplicationMatrixLine.propTypes = {
  commission: React.PropTypes.number,
  short: React.PropTypes.bool,
  matrix: React.PropTypes.array,
  title: React.PropTypes.string.isRequired,
};

export default ApplicationMatrixLine;
