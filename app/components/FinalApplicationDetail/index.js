/**
*
* FinalApplicationDetail
*
*/

import React from 'react';
import { FormattedNumber } from 'react-intl';

class FinalApplicationDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { applicants, businessName, loanAmount, repaymentAmount, sellRate, term, commission } = this.props.applicationDetails;
    return (
      <div className="finalization_text">
        <ul className="final-app">
          <li className="remove_padd_bottom">
            <span className="span_left ">Applicant Name</span>
            <span className="span_right"><b>{applicants.map((applicant, index) => (<label htmlFor="applicant" key={index}>{applicant}</label>))}</b></span>
          </li>
          <li>
            <span className="span_left">Business Name</span>
            <span className="span_right">{businessName}</span>
          </li>
          <li>
            <span className="span_left">Loan Amount</span>
            <span className="span_right"><FormattedNumber
              minimumFractionDigits={0}
              maximumFractionDigits={0}{...{ value: loanAmount, style: 'currency', currency: 'USD', maximumFractionDigits: 2 }}
            /></span>
          </li>
          <li>
            <span className="span_left"> Repayment Amount</span>
            <span className="span_right"><FormattedNumber
              minimumFractionDigits={0}
              maximumFractionDigits={0} {...{ value: repaymentAmount, style: 'currency', currency: 'USD', maximumFractionDigits: 2 }}
            /></span>
          </li>
          <li>
            <span className="span_left"> Sell Rate</span>
            <span className="span_right"><FormattedNumber {...{ value: sellRate, style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2 }} /></span>
          </li>
          <li>
            <span className="span_left"> Term</span>
            <span className="span_right">{term} days</span>
          </li>
          <li>
            <span className="span_left"><b>Commission </b></span>
            <span className="span_right"> <b><FormattedNumber
              minimumFractionDigits={0}
              maximumFractionDigits={0} {...{ value: commission, style: 'currency', currency: 'USD', maximumFractionDigits: 2 }}
            /></b> </span>
          </li>
        </ul>
      </div>
    );
  }
}

FinalApplicationDetail.propTypes = {
  applicationDetails: React.PropTypes.object.isRequired,
};

export default FinalApplicationDetail;
