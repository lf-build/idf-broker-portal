/**
*
* ClientAccountLinking
*
*/

import React from 'react';
import linkingIcon from 'assets/images/linking-icon.png';


class ClientAccountLinking extends React.Component { // eslint-disable-line react/prefer-stateless-function
  viewEmail=() => {
    const html = (this.props.emailBody && this.props.emailBody.length > 0) ?
    this.props.emailBody[this.props.emailBody.length - 1].body : 'No content';
    const newWindow = window.open();
    newWindow.document.write(html);
  }
  render() {
    const { bankLinkingStatus } = this.props;
    let activeClass = 'verify-box active';
    if (!bankLinkingStatus) {
      activeClass = 'verify-box';
    }
    return (
      <div className={activeClass}>
        <div className="number-box">
          <span>2</span>
        </div>
        <div className="verify-head">
          <h4>The link in the email will lead to the account linking page.</h4>
        </div>
        <div className="verify-img">
          <a className="cursor-default"><img src={linkingIcon} alt="icon" /></a>
        </div>
        <div className="verify-detail">
          <p>Have your client type in their Accounting &amp; Bank Log-in information.</p>
        </div>
      </div>
    );
  }
}

ClientAccountLinking.propTypes = {
  bankLinkingStatus: React.PropTypes.bool,
  emailBody: React.PropTypes.any,
};

export default ClientAccountLinking;
