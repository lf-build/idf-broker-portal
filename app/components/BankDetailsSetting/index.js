/**
*
* BankDetailsSetting
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { withRouter } from 'react-router';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import Form from '@ui/Form';
import SelectField from '@ui/SelectField';
import Spinner from 'components/Spinner';
import AlertMessage from 'components/AlertMessage';
import authCheck from 'components/Helper/authCheck';
import makeAuthSelector from 'sagas/auth/selectors';
import { required } from 'lib/validation/required';
import { isInLength } from 'lib/validation/isInLength';
import { isRoutingNumber } from 'lib/validation/isRoutingNumber';
import { isAccountNumber } from 'lib/validation/isAccountNumber';
import messages from './messages';

class BankDetailsSetting extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      partnerId: (this.props.auth && this.props.auth.user && this.props.auth.user.partnerId) ? this.props.auth.user.partnerId : undefined,
      isVisible: false,
    };
  }
  afterSubmitDone = (e, success) => {
    if (e) {
      this.setState({ isError: true, message: 'Something went wrong. Please try again later.', isVisible: true });
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      return;
    }
    this.setState({ isError: false, message: 'Bank details updated successfully.', isVisible: true, initialValues: success });
  };
  handleOnLoadError = (e) => {
    authCheck(this.props.dispatch)(e)(this.props.location.pathname);
  };
  render() {
    if (!this.state) {
      return <Spinner />;
    }
    const parsedMessages = localityNormalizer(messages);
    return (
      <div className="application_wraper">
        <div className="container-fluid responsive-wrap">
          <div className=" col-sm-12">
            <h2>Settings</h2>
          </div>
          <div className="res-margin">
            <div className="comman_applicationinner">
              <div className="col-md-5 col-sm-6">
                <Form initialValuesBuilder={(values) => this.state.initialValues || values} afterSubmit={this.afterSubmitDone} loadAction={['api/settings/get-bank-details', { partnerId: this.state.partnerId }]} name="bankDetails" action="api/settings/set-bank-details" onLoadError={this.handleOnLoadError}>
                  <TextField {...parsedMessages.bankName} name="bankName" validate={[required('Bank Name'), isInLength(2, 100)]} />
                  <TextField {...parsedMessages.bankAccountName} name="bankAccountName" validate={[required('Bank Account Name'), isInLength(2, 100)]} />
                  <TextField {...parsedMessages.bankAccountNumber} name="bankAccountNumber" validate={[required('Bank Account Number'), isAccountNumber('account Number')]} />
                  <SelectField {...parsedMessages.accountType} name="accountType" label="Account Type" source={[{ value: 1, text: 'Saving' }, { value: 2, text: 'Checking' }]} />
                  <TextField {...parsedMessages.aba} name="aba" validate={[required('ABA #'), isRoutingNumber('ABA number')]} />
                  <button type="submit" className="btn next_btn">UPDATE</button>
                  {this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
                  <TextField {...parsedMessages.bankId} name="bankId" type="hidden" />
                  <TextField {...parsedMessages.partnerId} name="partnerId" type="hidden" />
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BankDetailsSetting.propTypes = {
  auth: React.PropTypes.object,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(BankDetailsSetting));
