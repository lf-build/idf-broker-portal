/**
*
* PersonalDetailsSetting
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { IsUnderFileSizeLimit, IsValidImageFormat } from 'components/Helper/functions';
import { withRouter } from 'react-router';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import DateField from '@ui/DateField';
import PhoneField from '@ui/PhoneField';
import ZipField from '@ui/ZipField';
import Spinner from 'components/Spinner';
import { required } from 'lib/validation/required';
import { isPostalCode } from 'lib/validation/isPostalCode';
import { isMobile } from 'lib/validation/isMobile';
import { isDate } from 'lib/validation/isDate';
import { isInLength } from 'lib/validation/isInLength';
import Dropzone from 'react-dropzone';
import AlertMessage from 'components/AlertMessage';
import authCheck from 'components/Helper/authCheck';
import { executeAPI } from 'components/Helper/execute';
import makeAuthSelector from 'sagas/auth/selectors';
import { loadUserProfile } from 'sagas/auth/actions';
import userLogo from 'assets/images/man-user.png';
import messages from './messages';

const modifyJason = (values) => {
  const unFormatedMobileNum = values.mobileNumber.replace('(', '').replace(')', '').replace(/-/g, '').replace(/ /g, '');
  const request = {
    ...values,
    mobileNumber: unFormatedMobileNum,
  };
  return request;
};

class PersonalDetailsSetting extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, partnerId: (this.props.auth && this.props.auth.user && this.props.auth.user.partnerId) ? this.props.auth.user.partnerId : undefined };
  }
  afterSubmitHandle = (e, success) => {
    if (e) {
      this.setState({ isError: true, message: 'Something went wrong. Please try again later.', isVisible: true });
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      return;
    }
    this.setState({ isError: false, message: 'Personal details updated successfully.', isVisible: true, initialValues: success });
    this.props.dispatch(loadUserProfile({ userId: this.props.auth.user.userId, userName: this.props.auth.user.email, partnerId: this.props.auth.user.partnerId }));
  };
  handleOnLoadError = (e) => {
    authCheck(this.props.dispatch)(e)(this.props.location.pathname);
  };
  render() {
    const { auth: { user } } = this.props;
    if (!this.state) {
      return <Spinner />;
    }
    const handleFileDropped = (blobs) => {
      const promises = blobs.map((file) => new Promise((resolve) => {
        const reader = new FileReader();
        reader.onload = (e) => {
          resolve({ file: file.name, type: file.type, dataUrl: e.target.result, size: file.size });
        };
        reader.readAsDataURL(file);
      }));
      Promise.all(promises).then((...dataUrls) => onDrop(dataUrls));
    };
    const onDrop = (data) => {
      if (data[0].length > 1) {
        this.setState({ isError: true, message: 'Select only one image.', isVisible: true });
        return;
      }
      if (!IsValidImageFormat(data[0])) {
        this.setState({ isError: true, message: 'Please upload a JPG, JPEG or PNG image.', isVisible: true });
        return;
      }
      if (!IsUnderFileSizeLimit(data[0])) {
        this.setState({ isError: true, message: 'Maximum image size is 5MB.', isVisible: true });
        return;
      }
      const newFileObject = [];
      data[0].forEach((ele) => {
        newFileObject.push({ dataUrl: ele.dataUrl, file: ele.file.toLowerCase() });
      }, this);
      const payLoad = {
        userName: user.email,
        fileList: newFileObject,
      };
      executeAPI('api/settings/upload-profile-image', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
        if (response.error) {
        // add extra error handling code apart from authCheck
        } else {
          this.setState({ isError: false, message: 'Profile image updated successfully', isVisible: true });
          this.props.dispatch(loadUserProfile({ userId: this.props.auth.user.userId, userName: this.props.auth.user.email, partnerId: this.props.auth.user.partnerId }));
        }
      });
    };
    const parsedMessages = localityNormalizer(messages);
    let userImage = userLogo;
    if (this.state.partnerId && user.image) {
      userImage = `${user.image}`;
    }
    return (
      <div className="application_wraper">
        <div className="container-fluid responsive-wrap">
          <div className=" col-sm-12">
            <h2>Settings</h2>
          </div>
          <div className="res-margin">
            <div className="comman_applicationinner">
              <div className="col-md-7 col-sm-6 update-bx">
                <div className="uploadfile_section">
                  <h2> Update Profile Image</h2>
                  <a className="upload_file">
                    <img className="profileImg" src={`${userImage}?${Math.random()}`} alt="" />
                    <Dropzone multiple={false} className="dragfile_box_col" onDrop={(blobs) => handleFileDropped(blobs)}>
                      <span><i className="fa fa-upload" aria-hidden="true"></i>Upload a Profile Image</span>
                    </Dropzone>
                  </a>
                </div>
              </div>
              <div className="col-md-5 col-sm-6">
                <Form initialValuesBuilder={(values) => this.state.initialValues || values} afterSubmit={this.afterSubmitHandle} loadAction={['api/settings/get-personal-details', { partnerId: this.state.partnerId }]} payloadBuilder={modifyJason} name="personalDetails" action="api/settings/set-personal-details" onLoadError={this.handleOnLoadError}>
                  <TextField name="firstName" {...parsedMessages.firstName} validate={[required('First Name'), isInLength(2, 100)]} />
                  <TextField name="lastName" {...parsedMessages.lastName} validate={[required('Last Name'), isInLength(2, 100)]} />
                  <DateField dateFormat="mm/dd/yyyy" name="dateOfBirth" {...parsedMessages.dateOfBirth} label="Date of Birth" validate={[required('Date of Birth'), isDate('date')]} maxYears={100} />
                  <TextField name="street" {...parsedMessages.street} validate={[required('Street'), isInLength(2, 100)]} />
                  <TextField name="city" {...parsedMessages.city} validate={[required('City'), isInLength(2, 100)]} />
                  <ZipField name="postCode" {...parsedMessages.postCode} validate={[required('Post code'), isPostalCode('post code')]} />
                  <PhoneField name="mobileNumber" {...parsedMessages.mobileNumber} validate={[required('Mobile Phone Number'), isMobile('phone Number')]} />
                  <TextField name="companyName" {...parsedMessages.companyName} validate={[required('Company Name'), isInLength(2, 100)]} />
                  <button className="btn next_btn">UPDATE</button>
                  <TextField {...parsedMessages.partnerId} name="partnerId" type="hidden" />
                </Form>
                { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span /> }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PersonalDetailsSetting.propTypes = {
  auth: React.PropTypes.object,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(PersonalDetailsSetting));
