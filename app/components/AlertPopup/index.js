/**
*
* AlertPopup
*
*/

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { ModalContainer, ModalDialog } from 'react-modal-dialog';
import makeAuthSelector from 'sagas/auth/selectors';
import { loadUserProfile } from 'sagas/auth/actions';
import { createStructuredSelector } from 'reselect';
import { increaseTimeout } from 'sagas/session/actions';


class AlertPopup extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      isShowingModal: true,
    };
  }
  handleClick = () => this.setState({ isShowingModal: true });
  handleClose = () => this.setState({ isShowingModal: false });
  increaseTimer = () => {
    this.props.dispatch(increaseTimeout());
    this.props.dispatch(loadUserProfile({ userId: this.props.auth.user.userId, userName: this.props.auth.user.email, partnerId: this.props.auth.user.partnerId }));
    this.handleClose();
  }
  render() {
    return (
      <div>
        <button onClick={this.handleClick}>click</button>
        {
      this.state.isShowingModal &&
      <ModalContainer onClose={this.handleClose}>
        <ModalDialog onClose={this.handleClose}>
          <h3>Alert message</h3>
          <p>Your session will expire in {(parseInt(process.env.TOKEN_TIME_OUT, 10) - parseInt(process.env.TOKEN_THRESHOLD, 10)) / 60} minutes.</p>
          <button onClick={this.increaseTimer} className="btn button-b">Renew Session</button>
        </ModalDialog>
      </ModalContainer>
    }
      </div>);
  }
}

AlertPopup.propTypes = {
  dispatch: PropTypes.func.isRequired,
  auth: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AlertPopup);
