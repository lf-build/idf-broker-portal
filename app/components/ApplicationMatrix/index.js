/**
*
* ApplicationMatrix
*
*/

import React, { PropTypes } from 'react';
import ApplicationMatrixLine from 'components/ApplicationMatrixLine';
import Spinner from 'components/Spinner';
// import styled from 'styled-components';

class ApplicationMatrix extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    if (!this.props.dashboardMatrix) {
      return <Spinner />;
    }
    const { last30Days, lifeTime } = this.props.dashboardMatrix;
    const last30DaysMatrix = [{
      icon: 'widget_text widget_icon1',
      count: last30Days.applications,
      text: 'Applications',
    }, {
      icon: 'widget_text widget_icon2',
      count: last30Days.approved,
      text: 'Approved',
    }, {
      icon: 'widget_text widget_icon3',
      count: last30Days.funded,
      text: 'Funded',
    }];
    const last30DaysCommision = last30Days.commission;
    const lifetimeCommision = lifeTime.commission;
    const lifetimeMatrix = [{
      icon: 'widget_btn widget_btnicon1',
      count: lifeTime.applications,
      text: 'Applications',
    }, {
      icon: 'widget_btn widget_btnicon2',
      count: lifeTime.approved,
      text: 'Approved',
    }, {
      icon: 'widget_btn widget_btnicon3',
      count: lifeTime.funded,
      text: 'Funded',
    }];
    return (
      <div className="widget_wraper">
        <ApplicationMatrixLine matrix={last30DaysMatrix} commission={last30DaysCommision} title={'Last 30 days'} />
        <ApplicationMatrixLine matrix={lifetimeMatrix} commission={lifetimeCommision} short title={'Lifetime'} />
      </div>
    );
  }
}

ApplicationMatrix.propTypes = {
  dashboardMatrix: PropTypes.object,
};

export default ApplicationMatrix;
