/**
*
* PublicLayout
*
*/

import React from 'react';
// import styled from 'styled-components';
import Header from 'components/Header';

// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

class PublicLayout extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: React.PropTypes.node,
    // dispatch: PropTypes.func.isRequired,
  };
  render() {
    return (
      <div
        style={{
          height: '100%',
          position: 'absolute',
          width: '100%',
        }}
      >
        <Header emailMode />
        <div className="container-fluid">
          <div className="row">
            {React
            .Children
            .toArray(this.props.children)}
          </div>
        </div>
      </div>
    );
  }
}

PublicLayout.propTypes = {

};

export default PublicLayout;
