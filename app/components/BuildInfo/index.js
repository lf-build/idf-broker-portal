/**
*
* BuildInfo
*
*/

import React from 'react';
import styled from 'styled-components';
import { name, version } from '../../../package.json';

const VersionWrapper = styled.span`
  position: fixed;
  vertical-align: top;
  top: 5px;
  right: 20px;
  color: white;
  fontSize: 9px;
  line-height: 1;
  display: ${process.env.SHOW_VERSION_INFO ? 'block' : 'none'};
  z-index: 20;
`;

// Find a suitable way to show build numbers only during dev/qa/int.
// hidden={process.env.NODE_ENV !== 'development'}
const BuildInfo = () => <VersionWrapper> {name}@{version} </VersionWrapper>;

BuildInfo.propTypes = {

};

export default BuildInfo;
