/*
 * EmailLayout Messages
 *
 * This contains all the text for the EmailLayout component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.EmailLayout.header',
    defaultMessage: 'This is the EmailLayout component !',
  },
});
