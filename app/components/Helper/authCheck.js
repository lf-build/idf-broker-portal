import { clearState } from 'sagas/auth/actions';
// import { browserHistory } from 'react-router';

export default (dispatch) => (e) => (pathName) => {
  if (e && e.status && e.status.code === 401) { // If session expired
    dispatch(clearState(pathName));
  } /* else if (e && e.status && e.status.code === 400) { // For bad request
    browserHistory.replace('/dashboard');
  } */
};
