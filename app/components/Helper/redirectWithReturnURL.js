import { browserHistory } from 'react-router';

export default (pathName) => {
  browserHistory.replace(`/login?return_url=${pathName}`);
};
