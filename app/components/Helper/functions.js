/* export function downloadFile(fileName, content) {
  const a = document.createElement('a');
  const mimeType = 'data:application/octet-stream;base64,';
  const data = `${mimeType}${content}`;
  a.href = data;
  a.download = fileName;
  a.click();
} */

export function downloadFile(fileName, content) {
  // Content is base64
  const a = document.createElement('a');
  const url = window.URL.createObjectURL(b64toBlob(content));
  a.href = url;
  a.download = fileName;
  a.click();
  window.URL.revokeObjectURL(url);
}

function b64toBlob(b64Data) {
  const Size = 512;

  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += Size) {
    const slice = byteCharacters.slice(offset, offset + Size);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i += 1) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, { type: 'octet/stream' });
  return blob;
}

const defaultMaxFileSize = 5000000; // in bytes
export function IsUnderFileSizeLimit(fileList, maxSize) {
  let fileSizeLimit = defaultMaxFileSize;
  if (maxSize) {
    fileSizeLimit = maxSize;
  }
  for (let i = 0; i < fileList.length; i += 1) {
    const fileObject = fileList[i];
    if (fileObject.size > fileSizeLimit) {
      return false;
    }
  }
  return true;
}

const defaultValidFileFormat = ['image/png', 'image/jpeg', 'application/pdf'];
export function IsValidFileFormat(fileList) {
  for (let i = 0; i < fileList.length; i += 1) {
    const fileObject = fileList[i];
    if (defaultValidFileFormat.filter((p) => p === fileObject.type).length === 0) {
      return false;
    }
  }
  return true;
}

const defaultValidImageFormat = ['image/png', 'image/jpeg'];
export function IsValidImageFormat(fileList) {
  for (let i = 0; i < fileList.length; i += 1) {
    const fileObject = fileList[i];
    if (defaultValidImageFormat.filter((p) => p === fileObject.type).length === 0) {
      return false;
    }
  }
  return true;
}

export function getInitialName(fullName) {
  let response = '';
  if (fullName) {
    const splitedName = fullName.split(' ');
    response = splitedName.length === 0 ? '' : `${splitedName.length > 0 ? splitedName[0][0] : ''}${splitedName.length > 1 ? splitedName[1][0] : ''}`;
  }
  return response;
}
