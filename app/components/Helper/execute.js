import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';

export function executeAPI(apiURL, payLoad, pathName, dispatch, checkAuth, result) {
  execute(undefined, undefined, ...apiURL.split('/'), payLoad)
    .then((body) => {
      result({ error: false, data: body });
    }).catch((e) => {
      if (checkAuth) {
        authCheck(dispatch)(e)(pathName);
      }
      result({ error: true, data: e });
    });
}

