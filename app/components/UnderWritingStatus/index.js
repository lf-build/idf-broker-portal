/**
*
* UnderWritingStatus
*
*/

import React from 'react';
import settingIcon from 'assets/images/setting-icon.png';

class UnderWritingStatus extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { underWritingStatus } = this.props;
    let activeClass = 'verify-box active';
    if (!underWritingStatus) {
      activeClass = 'verify-box';
    }
    return (
      <div className={activeClass}>
        <div className="number-box">
          <span>3</span>
        </div>
        <div className="verify-head">
          <h4>Underwriting will begin when all accounts are linked.</h4>
        </div>
        <div className="verify-img">
          <a className="cursor-default"><img src={settingIcon} alt="" /></a>
        </div>
        <div className="verify-detail">
          <p>The application process will continue automatically once underwriting is complete.</p>
        </div>
      </div>
    );
  }
}

UnderWritingStatus.propTypes = {
  underWritingStatus: React.PropTypes.bool,
};

export default UnderWritingStatus;
