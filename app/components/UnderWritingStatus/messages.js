/*
 * UnderWritingStatus Messages
 *
 * This contains all the text for the UnderWritingStatus component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.UnderWritingStatus.header',
    defaultMessage: 'This is the UnderWritingStatus component !',
  },
});
