/**
*
* LinkBankAccount
*
*/

import React from 'react';
import { executeAPI } from 'components/Helper/execute';
import ssl from '../../assets/images/ssl.png';
import soc from '../../assets/images/soc.jpg';

class LinkBankAccount extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    window.applicationNumber = this.props.location.pathname.split('/')[3];
    this.handler = Plaid.create({ // eslint-disable-line
      clientName: 'broker portal',
      env: 'tartan',
      key: 'test_key', // Replace with your public_key to test with live credentials
      product: ['connect'],
    // webhook: '[WEBHOOK_URL]', // Optional – use webhooks to get transaction and error updates
      selectAccount: true, // Optional – trigger the Select Account
      onLoad: () => {
      // Optional, called when Link loads
      },
      onSuccess : (publicToken, metadata)=> { // eslint-disable-line
        this.props.apiCallStatus(1);
        const payload = metadata;
        payload.applicationNumber = window.applicationNumber;
        executeAPI('api/account-linking/set-bank-account', payload, undefined, undefined, false, (response) => {
          if (response.error) {
            // error handling
          } else {
            setTimeout(() => this.props.loadData(), 2000); // All done page will be rend
          }
        });
      },
      onExit : (err, metadata) => { // eslint-disable-line
        if (err != null) {
        // todo
        }
      },
    });
  }


  openPlaidWidget=() => {
    this.handler.open();
  }
  render() {
    return (
      <div className="col-md-6 account-contain">
        <div className="que-heading">
          <h2>Business Verification</h2>
        </div>
        <div className="progress-bars">
          <span className="circle">1</span>
          <span className="line-d"></span>
          <span className="circle active">2</span>
          <span className="line-d"></span>
          <span className="circle">3</span>
        </div>
        <div className="col-md-12">
          <div className="link-wrap">
            <div className="col-sm-7">
              <h4 className="ac-head">Business Checking Account Review</h4>
              <p className="ac-text">we look at your business data, not just your credit score, to get you qualified quickly and easily.</p>
              <button onClick={this.openPlaidWidget} className="account_btn">{ this.props.apiCall ? 'CONNECTING...' : 'CONNECT'}</button>
            </div>
          </div>
        </div>
        <div className="col-md-12">
          <div className="privacy-wrap">
            <div className="privacy-imgs col-sm-2">
              <div className="pri-logo">
                <img src={soc} alt="" />
              </div>
              <div className="ssl-logo">
                <img src={ssl} alt="SSL" />
              </div>
            </div>
            <div className="privacy-text col-sm-10">
              <h4>Privacy notice</h4>
              <p>Divergent Capital uses Plaid Technologies, Inc. (“Plaid”) to gather your data from financial institutions. By using our service, you grant Client and Plaid the right, power, and authority to act on your behalf to access and transmit your personal and financial information from the relevant financial institution. You agree to your personal and financial information being transferred, stored, and processed by Plaid in accordance with the <a target="_blank" href=" https://plaid.com/legal"> Plaid Privacy Policy</a>.</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

LinkBankAccount.propTypes = {
  location: React.PropTypes.any,
  loadData: React.PropTypes.func,
  apiCall: React.PropTypes.any,
  apiCallStatus: React.PropTypes.func,
};

export default LinkBankAccount;
