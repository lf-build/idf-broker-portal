/**
*
* CreditReview
*
*/

import React from 'react';
import { Field } from 'redux-form/immutable';
import Form from '@ui/Form';
import PrivacyPolicy from 'components/PrivacyPolicy';
// import styled from 'styled-components';

const renderConsentField = ({ input, label, type, meta: { touched, error, warning } }) => ( // eslint-disable-line
  <div className="ac-text">
    <p htmlFor="consents"> <input className="chkBox" {...input} placeholder={label} type={type} />I hereby authorize DivergentCapital, Inc. and its agents to obtain an
investigative or consumer report from a credit bureau or a credit agency and to
investigate the references given on any other statement or data obtained, and
I agree to the <a target="_blank" href="http://divergecap.com/terms-of-use/"> Terms of Service </a>&amp; <a target="_blank" href="http://divergecap.com/privacy-policy/"> Privacy Policy.</a> </p>
    {touched &&
        ((error && <span className="error_message">{error}</span>) ||
          (warning && <span>{warning}</span>))}
  </div>
);

const validate = (_values) => {
  const errors = {};
  const values = _values.toJS();
  if (!values.creditReview) {
    errors.creditReview = 'Please check consent checkbox';
  }
  return errors;
};
class CreditReview extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const modifyJason = () => {
      const payload = { applicationNumber: this.props.location.pathname.split('/')[3] };
      this.props.apiCallStatus(1);
      return payload;
    };
    const afterSubmit = (err) => {
      if (err) {
        this.props.apiCallStatus(3);
      } else {
        setInterval(this.props.loadData(), 1000);
      }
    };

    return (
      <div className="col-md-6 account-contain">
        <div className="que-heading">
          <h2>Business Verification</h2>
        </div>
        <div className="progress-bars">
          <span className="circle">1</span>
          <span className="line-d"></span>
          <span className="circle">2</span>
          <span className="line-d"></span>
          <span className="circle active">3</span>
        </div>
        <div className="col-md-12">
          <Form validate={(values) => validate(values)} afterSubmit={afterSubmit} name="creditReviewForm" action="api/account-linking/sign-credit-review-consent" payloadBuilder={modifyJason}>
            <div className="link-wrap">
              <div className="col-sm-9">
                <h4 className="ac-head">Credit Review</h4>
                <Field name="creditReview" component={renderConsentField} type="checkbox" />
                <button type="submit" className="account_btn">{ this.props.apiCall ? 'SIGNING...' : 'FINISH'}</button>
              </div>
            </div>
          </Form>
        </div>
        <PrivacyPolicy />
      </div>
    );
  }
}

CreditReview.propTypes = {
  location: React.PropTypes.any,
  loadData: React.PropTypes.func,
  apiCall: React.PropTypes.any,
  apiCallStatus: React.PropTypes.func,
};

export default CreditReview;
