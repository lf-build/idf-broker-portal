/*
 * Files Messages
 *
 * This contains all the text for the Files component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Files.header',
    defaultMessage: 'This is the Files component !',
  },
});
