/**
*
* Files
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import { downloadFile } from 'components/Helper/functions';
import { executeAPI } from 'components/Helper/execute';
import closeIcon from 'assets/images/close-img.png';
import fileIcon from 'assets/images/file.png';
import Spinner from 'components/Spinner';

class Files extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showSpinner: false };
  }
  showSpinner() {
    this.setState({ showSpinner: true });
  }
  hideSpinner() {
    this.setState({ showSpinner: false });
  }
  downloadDocument(fileId, fileName) {
    this.showSpinner();
    const payLoad = {
      applicationNumber: this.props.applicationNumber,
      fileId,
    };
    executeAPI('api/agreement/download-file', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
        this.hideSpinner();
      } else {
        downloadFile(fileName, response.data.body.downloadString).then(() => this.hideSpinner());
      }
    });
  }
  discardDocument(fileId, uploadFileType) {
    this.showSpinner();
    const payLoad = {
      applicationNumber: this.props.applicationNumber,
      fileId,
      fileType: uploadFileType,
    };
    executeAPI('api/agreement/discard-document', payLoad, this.props.location.pathname, this.props.dispatch, true, (response) => {
      if (response.error) {
        this.hideSpinner();
      } else {
        this.props.onDiscardFile().then(() => this.hideSpinner());
      }
    });
  }
  render() {
    const { files, uploadFileType } = this.props;
    return (
      <div>
        {files.map((file) => (
          <div className="id-icon" key={file.fileId}>
            <span><img src={fileIcon} alt="" /></span>
            <Link onClick={() => this.downloadDocument(file.fileId, file.fileName)} className="tooltip-b">
              <label htmlFor="file-name" className="file-name">{file.fileName}</label>
              <span className="tooltiptext">{file.fileName}</span>
            </Link>
            <Link onClick={() => this.discardDocument(file.fileId, uploadFileType)}><img src={closeIcon} alt="" /></Link>
          </div>
         ))}
        { this.state.showSpinner && <Spinner /> }
      </div>
    );
  }
}

Files.propTypes = {
  files: React.PropTypes.any,
  applicationNumber: React.PropTypes.string,
  onDiscardFile: React.PropTypes.func.isRequired,
  uploadFileType: React.PropTypes.string.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(withRouter(Files));
