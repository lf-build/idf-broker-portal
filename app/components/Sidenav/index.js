/**
*
* Sidenav
*
*/

import React from 'react';
import { Link, withRouter } from 'react-router';
import { connect } from 'react-redux';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import { createStructuredSelector } from 'reselect';
import Spinner from 'components/Spinner';
import { getInitialName } from 'components/Helper/functions';
import menuIcon from 'assets/images/menu-icon.png';
import makeAuthSelector from 'sagas/auth/selectors';


class Sidenav extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { labelClassName: '', uiClassName: 'submenu' };
  }
  componentDidMount() {
    if (!this.props.auth || !this.props.auth.user) {
      redirectWithReturnURL(this.props.location.pathname);
    }
    if (this.props.location.pathname === '/settings/account' || this.props.location.pathname === '/settings/personal' || this.props.location.pathname === '/settings/bank') {
      this.setState({ labelClassName: 'open', uiClassName: 'submenu open' }); // eslint-disable-line
    }
  }
  toggleClass() {
    if (this.state.labelClassName === 'open') {
      this.setState({ labelClassName: '', uiClassName: 'submenu' });
    } else {
      this.setState({ labelClassName: 'open', uiClassName: 'submenu open' });
    }
  }
  render() {
    if (!this.props.auth || !this.props.auth.user) {
      return <Spinner />;
    }
    const { auth: { user } } = this.props;
    const initialName = getInitialName(user.name);
    return (
      <nav className="sidebar col-lg-2 col-md-3">
        <label htmlFor="menu-toggle" id="menu-res"><img src={menuIcon} alt="" /></label>
        <input type="checkbox" id="menu-toggle" />
        <div id="menus">
          <div className="side-nav">
            <div className="logo-panel">
              {user.image && <Link className="b-logo" to="/settings/personal"><img src={`${user.image}?${Math.random()}`} alt="" /></Link>}
              {!user.image && <Link className="b-logo" to="/settings/personal"><span className="logo-text">{initialName}</span></Link>}
              <div className="brand-detail">
                <h3 className="brand_name">{user.name}</h3>
                <a className="brand_email">{user.email}</a>
              </div>
            </div>
            <div className="brand-btns">
              <Link to="/application/new" className="brand_btn">New Application</Link>
            </div>
          </div>
          <div className="clearfix"></div>
          <div className="left-menu">
            <ul id="menu-wrap">
              <li> <Link to="/dashboard">Dashboard</Link></li>
              <li>
                <Link className="settingLink" onClick={() => this.toggleClass()}><label htmlFor="check02" className={this.state.labelClassName}>Settings</label></Link>
                <ul className={this.state.uiClassName}>
                  <li><Link to="/settings/personal">Personal Details</Link></li>
                  <li><Link to="/settings/bank">Bank Details</Link></li>
                  <li><Link to="/settings/account">Account Details</Link></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

Sidenav.propTypes = {
  auth: React.PropTypes.object,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

export default connect(mapStateToProps)(withRouter(Sidenav));
