/**
*
* PrivacyPolicy
*
*/

import React from 'react';
import ssl from '../../assets/images/ssl.png';
import soc from '../../assets/images/soc.jpg';
// import styled from 'styled-components';


class PrivacyPolicy extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className="col-md-12">
        <div className="privacy-wrap">
          <div className="privacy-imgs col-sm-2">
            <div className="pri-logo">
              <img src={soc} alt="" />
            </div>
            <div className="ssl-logo">
              <img src={ssl} alt="SSL" />
            </div>
          </div>
          <div className="privacy-text col-sm-10">
            <h4>Privacy notice</h4>
            <p>Your information is safe , secure, and private. Divergent capital does not view or store your login information. if you’d like to learn more about this process, please check our <a target="_blank" href="https://divergecap.com/customer-faq/">Customer FAQ</a></p>
          </div>
        </div>
      </div>
    );
  }
}

PrivacyPolicy.propTypes = {

};

export default PrivacyPolicy;
