/*
 *
 * Session constants
 *
 */

export const PAUSE = 'PAUSE_SESSION_TIMER';
export const SHOW_POPUP = 'SHOW_POPUP';
export const START = 'START_SESSION_TIMER';
export const INCREASE_TIMEOUT = 'INCREASE_TIMEOUT';
