/*
 *
 * Session actions
 *
 */

import {
  PAUSE,
  SHOW_POPUP,
  START,
  INCREASE_TIMEOUT,
} from './constants';

export function pauseSessionTimer() {
  return {
    type: PAUSE,
  };
}

export function showPopup() {
  return {
    type: SHOW_POPUP,
  };
}

export function startSessionTimer() {
  return {
    type: START,
  };
}

export function increaseTimeout() {
  return {
    type: INCREASE_TIMEOUT,
  };
}

