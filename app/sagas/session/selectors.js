import { createSelector } from 'reselect';

/**
 * Direct selector to the auth state domain
 */
const traceDomain = () => (state) => state.get('trace');

/**
 * Other specific selectors
 */


/**
 * Default selector used by SocialPage
 */

const makeTrace = () => createSelector(
  traceDomain(),
  (substate) => substate.toJS()
);

export default makeTrace;
export {
  traceDomain,
};
