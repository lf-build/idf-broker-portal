/*
 *
 * auth reducer
 *
 */

import { fromJS } from 'immutable';
import {
  PAUSE,
  SHOW_POPUP,
  START,
  INCREASE_TIMEOUT,
} from './constants';

const traceInitialState = fromJS({ time: new Date(), timeout: process.env.TOKEN_TIME_OUT });

function traceReducer(state = traceInitialState, action) {
  switch (action.type) {
    case SHOW_POPUP:
      return state.set('show', true);
    case PAUSE:
      return state.set('enabled', false).set('time', new Date());
    case START:
      return state.set('enabled', true).set('time', new Date());
    case INCREASE_TIMEOUT:
      return state.set('show', false);
    default:
      return state.set('time', new Date());
  }
}

export default traceReducer;
