import { put, select } from 'redux-saga/effects';
// import { browserHistory } from 'react-router';
// import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
// import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import { userSignedOut } from '../auth/actions';
import { PAUSE, SHOW_POPUP } from './constants';
import makeTraceSelector from './selectors';

// const timeout = 50;
const threshold = process.env.TOKEN_THRESHOLD;
const interval = process.env.TOKEN_LIVE_CHECK_INTERVAL;

function* listen() {
  const state = yield select(makeTraceSelector());
  if (!state.enabled) {
    return sleep();
  }
  const idealTime = Math.ceil((new Date() - state.time) / 1000);

  if (idealTime >= state.timeout) {
    // console.log('Do a logout');
    yield put({
      type: PAUSE,
    });
    yield put(userSignedOut());
  }
  if (idealTime >= threshold) {
    // console.log('Show popup');
    yield put({
      type: SHOW_POPUP,
    });
  }
  return null;
  // console.log(`Ideal for: ${idealTime}`);
}

function* sleep() {
  return yield new Promise((resolve) => {
    setTimeout(() => resolve({}), interval * 1000);
  });
}

// Individual exports for testing
export function* defaultSaga() {
  while (true) { // eslint-disable-line
    yield sleep();
    yield listen();
  }
}

// All sagas to be loaded
export default [
  defaultSaga,
];
