import { takeEvery, put, select } from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import { userInfoAvailable } from './actions';
import { LOAD_PROFILE, USER_SIGNED_OUT } from './constants';
import makeAuthSelector from '../../sagas/auth/selectors';

function* loginSaga(action) {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'authorization',
    section: 'identity',
    command: 'who-am-i',
  }, {
    tag: 'who-am-i',
    payload: {
      userId: action.meta.userId,
      userName: action.meta.userName,
      partnerId: action.meta.partnerId,
    },
  }))) {
    return;
  }

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('who-am-i');
  if (valid) {
    yield put(userInfoAvailable(valid.meta.body));
  } else {
    throw new Error('Invalid token');
  }
}

function* logoutSaga() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'authorization',
    section: 'identity',
    command: 'logout',
  }, {
    tag: 'sign-out',
  }))) {
    return;
  }
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('sign-out');
  if (valid) {
    yield put({
      type: 'RESET',
      state: undefined,
    });
    localStorage.clear();
    browserHistory.replace('/');
  } else {
    throw new Error('Invalid token');
  }
}

function* checkSession() {
  const pathName = browserHistory.getCurrentLocation().pathname;
  if (pathName === '/login' || pathName === '/forgot-password' || pathName === '/reset-account') {
    return;
  }
  const auth = (yield select(makeAuthSelector()));
  if (auth && auth.token) {
    return;
  }
  setTimeout(() => {
    // browserHistory.replace('/login');
  }, 1);
}

function* signedOut(action) {
  const pathName = browserHistory.getCurrentLocation().pathname;
  if (action.meta.pathName) {
    redirectWithReturnURL(action.meta.pathName);
  } else {
    if (pathName === '/login') {
      return;
    }
    browserHistory.replace('/login');
  }
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery(LOAD_PROFILE, loginSaga);
  yield takeEvery(USER_SIGNED_OUT, logoutSaga);
  yield takeEvery('@@router/LOCATION_CHANGE', checkSession);
  yield takeEvery('app/SignInPage/CLEAR_STATE', signedOut);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
